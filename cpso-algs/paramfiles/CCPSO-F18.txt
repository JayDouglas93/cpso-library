#GENERAL PARAMS

iterations = 1000
functionEvals = 10000
runs = 1
dimensions = 30
numParticles = 10
kParts= 6
singleRun = true
logging = true
functionDir = cecfunctions2010

algorithms = CCPSO
functions = F18
rotated = false

#VELOCITY PARAMS
c1 = 1.496180
c2 = 1.496180
inertia = 0.729844

#CCPSO
regroupEvery = 1
numLoopWeight = 3

#MCPSO
numMerge = 2
freqMerge = 0
pgBestParamM = 1
maxStagnateM = 5
regroupMethodM = 1

#DCPSO
numSplit = 2
freqSplit = 0
pgBestParamD = 1
maxStagnateD = 5
regroupMethodD = 1

#CCPSO2
cauchyProb = 0.5
swarmSizes = 2, 5, 50, 100, 200
maxSameFit = 3

#AMCCPSO
numContextVectors = 6
numSPSC = 6
probGlobalBest = 0.33
probSubSwarmBest = 0.33
# Note: 1 - probGlobalBest - probSubSwarmBest = probability of neighborhood best
 
#STATISTICAL RUN
compileRuns = 1