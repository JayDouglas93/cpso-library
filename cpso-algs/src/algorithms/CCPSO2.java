package algorithms;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;


public class CCPSO2 extends Algorithm {

	private double[] contextVector;
	private Swarm[] swarms;
	private SwarmManager swarmManager;

	private double cauchyProb;
	private int kParts;
	private int curS;
	private int regroupRK;

	private int[] swarmOfDim;

	private int[] swarmSizes;

	private double lastFitness;
	private int countSameFit; // What's current # of stagnated iterations
	private int maxSameFit; // How many stagnated iterations until you act on it

	private int curFnEval;

	public CCPSO2(HashMap<String, String> p) {

		super(p); // Sends HashMap to superclass who then creates global,
					// protected variables

		contextVector = new double[dimensions];

		kParts = Integer.parseInt(p.get("kParts"));
		regroupRK = Integer.parseInt(p.get("regroupEvery"));
		cauchyProb = Double.parseDouble(p.get("cauchyProb"));

		fitPerEval = new double[fnEvals];

		swarmOfDim = new int[dimensions];

		String[] temp = p.get("swarmSizes").split(",");
		swarmSizes = new int[temp.length];
		for (int s = 0; s < temp.length; s++) {
			swarmSizes[s] = Integer.parseInt(temp[s].trim());
		}

		maxSameFit = Integer.parseInt(p.get("maxSameFit"));
		countSameFit = 0;

		int personalGlobalBest = Integer.parseInt(p.get("pgBestParamM"));
		swarmManager = new SwarmManager(personalGlobalBest, numParticles, fn);

		setRandomS();
		initSwarms();
		begin();
	}

	public void begin() {

		curFnEval = 0;
		//int counter = 0;
		while (!endCondition(curFnEval)) {

			if (logging) {
				System.out.println("Iteration " + curFnEval);

				for (int q = 0; q < dimensions; q++) {
					System.out.println("Dimension " + q + " is " + contextVector[q]);
				}
				System.out.println("...");
			}

			// CHECK LAST FITNESS, RESTRUCTURE IF NO IMPROVEMENT
			// POSITIONS ARE RANDOMLY RESET FOR NEW SUBSWARMS
			checkImproved();
			lastFitness = fn.compute(contextVector);
			if (countSameFit == maxSameFit) { // set random s,
				if (logging) {
					System.out.println("Fitness stagnated for " + countSameFit + " iterations, regrouping...");
				}
				setRandomS(); // kParts will be updated with new random s

				// Used to determine new structure
				int k1 = dimensions % kParts;
				int s1 = (int) Math.ceil((double) dimensions / (double) kParts);
				int s2 = (int) Math.floor((double) dimensions / (double) kParts);

				int[] dimPerSwarm = new int[kParts];

				for (int s = 0; s < kParts; s++) {
					if (s < k1) {
						dimPerSwarm[s] = s1;
					} else {
						dimPerSwarm[s] = s2;
					}
				}

				swarms = swarmManager.changeK(swarms, dimensions, dimPerSwarm);

				/*
				 * for (int i=0 ; i < swarms.length ; i++){ for (int d=0 ;
				 * d<swarms[i].getNumDims() ; d++){
				 * System.out.print("Swarm "+i+": "+swarms[i].getIndex(d)+", ");
				 * } System.out.println(); }
				 */

				countSameFit = 0;
				updateSwarmOfDim();
				
				randomRegroup();
				
			}

			// RANDOMLY REGROUP DIMENSIONS OF EACH SUBSWARM
			//if (counter == regroupRK) {

			//	randomRegroup();

			//	counter = 0;
			//}

			loopCCPSO2();

			// OLD PLACE FOR REGROUPING

			//counter += 1;
		}

	}

	private void loopCCPSO2() {

		double[][] vectorEvals = new double[numParticles][dimensions];

		for (int i = 0; i < kParts; i++) {

			Swarm curSwarm = swarms[i];

			for (int j = 0; j < numParticles; j++) {

				Particle curParticle = curSwarm.getParticle(j);

				double[] curPos = curParticle.getCurrentPosition();
				double[] bestPos = curParticle.getBestPosition();

				if (fitness(curSwarm, contextVector, curPos) < fitness(curSwarm, contextVector, bestPos)) {
					curParticle.setBestPosition(curPos);
				}

				bestPos = curParticle.getBestPosition();
				double bestPosFit = fitness(curSwarm, contextVector, bestPos);

				double[] swarmBestPos = curSwarm.getBestPosition();

				if (bestPosFit < fitness(curSwarm, contextVector, swarmBestPos)) {
					curSwarm.setGlobalBest(bestPos, j);

					if (bestPosFit < fn.compute(contextVector)) {
						for (int k = 0; k < curSwarm.getNumDims(); k++) {

							contextVector[curSwarm.getIndex(k)] = bestPos[k];
						}
					}
				}
				vectorEvals[j] = contextVector.clone();
				/*
				 * for (int k = 0; k < curSwarm.getNumDims(); k++) {
				 * vectorEvals[j][curSwarm.getIndex(k)] =
				 * contextVector[curSwarm.getIndex(k)]; }
				 */

			}
			// Update each particle with their local best, BUT you could just do
			// it on the fly later

			// Update context vector only once you process all particles and
			// find true swarm best, then only
			// update once you confirm swarm best is better then whatever is in
			// context vector
			// System.out.println(fitness(swarms, i,
			// swarms[i].getBestPosition()) +" < "+fn.compute(contextVector));

			// OLD POS VEL UPDATE

			for (int j = 0; j < numParticles; j++) {
				updatePosition(swarms[i], j);
			}
		}

		/*
		 * if (curFnEval < fnEvals){ double fnFitness =
		 * fn.compute(contextVector);
		 * 
		 * fitPerEval[curFnEval] = fnFitness;
		 * 
		 * curFnEval += 1; }
		 */
		for (int j = 0; j < numParticles; j++) {
			if (curFnEval < fnEvals) {
				fitPerEval[curFnEval] = fn.compute(vectorEvals[j]);
				curFnEval += 1;
			}
		}

		// FEB 2018, POS, VEL
	}

	public void initSwarms() {

		swarms = new Swarm[kParts];
		int[] dimPerSwarm = initDimPerSwarm();

		for (int i = 0; i < kParts; i += 1) {
			swarms[i] = new Swarm(numParticles, dimPerSwarm[i], fn);
		}

		initDimensions();

		for (int i = 0; i < kParts; i += 1) {
			// Initialize context vector to random particles in each swarm
			Random rand = new Random();
			int index = rand.nextInt(numParticles);
			double[] swarmVal = swarms[i].getParticle(index).getCurrentPosition();

			for (int j = 0; j < dimPerSwarm[i]; j++) {
				contextVector[swarms[i].getIndex(j)] = swarmVal[j];
			}
		}

		lastFitness = fn.compute(contextVector);

	}

	public void initDimensions() {

		Random rnd = ThreadLocalRandom.current();
		ArrayList<Integer> nums = new ArrayList<Integer>();
		for (int i = 0; i < dimensions; i++) {
			nums.add(i);
		}

		int k1 = dimensions % kParts;
		int k2 = kParts - (dimensions % kParts);

		int dimK1 = (int) Math.ceil((double) dimensions / (double) kParts);
		int dimK2 = (int) Math.floor((double) dimensions / (double) kParts);

		for (int i = 0; i < k1; i++) {
			for (int j = 0; j < dimK1; j++) {

				int whichIndex = nums.remove(rnd.nextInt(nums.size()));

				swarms[i].setIndex(j, whichIndex);
				//System.out.print("S " + i + " --> " + swarms[i].getIndex(j) + ", ");
				swarmOfDim[whichIndex] = i;
			}
			// System.out.println();
		}

		for (int i = k1; i < kParts; i++) {
			for (int j = 0; j < dimK2; j++) {

				int whichIndex = nums.remove(rnd.nextInt(nums.size()));

				swarms[i].setIndex(j, whichIndex);
				//System.out.print("S " + i + " --> " + swarms[i].getIndex(j) + ", ");
				swarmOfDim[whichIndex] = i;
			}
			// System.out.println();
		}

		/*
		 * for (int i=0 ; i < swarms.length ; i++){ for (int d=0 ;
		 * d<swarms[i].getNumDims() ; d++){
		 * System.out.print("Swarm "+i+": "+swarms[i].getIndex(d)+", "); }
		 * System.out.println(); }
		 */

		//System.out.println("\n");
	}

	public int[] initDimPerSwarm() {

		int[] dimPerSwarm = new int[kParts];

		int k1 = dimensions % kParts;
		int k2 = kParts - (dimensions % kParts);

		int dimK1 = (int) (Math.ceil((double) dimensions / (double) kParts));
		int dimK2 = (int) Math.floor((double) dimensions / (double) kParts);

		//System.out.println(dimK1 + " " + dimK2);

		for (int i = 0; i < k1; i++) {
			dimPerSwarm[i] = dimK1;
			//System.out.print(dimK1 + ", ");
		}

		for (int i = k1; i < kParts; i++) {
			dimPerSwarm[i] = dimK2;
			//System.out.print(dimK2 + ", ");
		}

		//System.out.println();

		return dimPerSwarm;
	}

	public void updatePosition(Swarm s, int i) {

		double[] partBestPos = s.getParticle(i).getBestPosition().clone();
		double[] lBest = getLBest(s, i).clone();

		int dims = 0;

		dims = s.getNumDims();

		double[] newPos = new double[dims];

		Random r = new Random();
		double rand = r.nextDouble(); // (0,1)
		double coef = 0;
		double[] prevPos = null;

		if (rand <= cauchyProb) {
			coef = cauchyDist();
			prevPos = partBestPos;
		} else {
			r = new Random();
			coef = r.nextGaussian();
			prevPos = lBest;

		}

		for (int j = 0; j < dims; j++) {
			newPos[j] = prevPos[j] + coef * (Math.abs(partBestPos[j] - lBest[j]));
			double pMax = s.getPMax();
			double pMin = s.getPMin();

			if (newPos[j] > pMax) {
				newPos[j] = pMax;
			} else if (newPos[j] < pMin) {
				newPos[j] = pMin;
			}
		}
		s.getParticle(i).setCurrentPosition(newPos);

	}

	public Swarm[] copySwarms() {

		Swarm[] copySwarms = new Swarm[swarms.length];
		for (int s = 0; s < swarms.length; s++) {

			Swarm swrm = swarms[s];

			copySwarms[s] = new Swarm(swrm);

		}
		return copySwarms;
	}

	/**
	 * Used randomly reshuffle the dimensions amongst the sub-swarms
	 */
	public void randomRegroup() {

		//System.out.println("Regrouping");

		int[] prevSwarmOfDim = swarmOfDim.clone();
		Swarm[] oldSwarms = copySwarms();

		initDimensions();

		for (int i = 0; i < kParts; i++) {

			updatePBests(i, prevSwarmOfDim, oldSwarms);
			updateGlobalBests(i, prevSwarmOfDim, oldSwarms);
			// The swarm will look to new indicies in the context vector now,
			// so...
			// there is no need to update the values since the appropriate
			// values
			// will be changed anyways
		}

	}

	public void updatePBests(int i, int[] prevSwarmOfDim, Swarm[] oldSwarms) {
		Swarm curSwarm = swarms[i];
		int[] curSwarmDims = curSwarm.getDimIndices();

		Swarm prevSwarm;
		int[] prevSwarmDims;
		double[] prevSwarmPBest;

		for (int p = 0; p < numParticles; p++) {
			double[] newPBest = new double[curSwarm.getNumDims()];
			for (int j = 0; j < curSwarm.getNumDims(); j++) {
				int curIndex = curSwarmDims[j];
				int prevIndex = 0;
				int prevSwarmIndex = prevSwarmOfDim[curIndex];

				// previous swarm looking at the current dimension at index j
				// System.out.println("legnth of old swarms:
				// "+oldSwarms.length);
				prevSwarm = oldSwarms[prevSwarmIndex];

				// error: using updated dim indices
				prevSwarmDims = prevSwarm.getDimIndices();
				prevSwarmPBest = prevSwarm.getParticle(p).getBestPosition();
				for (int k = 0; k < prevSwarm.getNumDims(); k++) {

					if (prevSwarmDims[k] == curIndex) {
						prevIndex = k;
					}
				}
				newPBest[j] = prevSwarmPBest[prevIndex];

			}
			curSwarm.getParticle(p).setBestPosition(newPBest);
		}
	}

	public void updateGlobalBests(int i, int[] prevSwarmOfDim, Swarm[] oldSwarms) {

		Swarm curSwarm = swarms[i];
		int[] curSwarmDims = curSwarm.getDimIndices();

		Swarm prevSwarm;
		int[] prevSwarmDims;

		double[] prevGBest;

		double[] newGBest = new double[curSwarm.getNumDims()];

		for (int j = 0; j < curSwarm.getNumDims(); j++) {
			int curIndex = curSwarmDims[j];
			int prevIndex = 0;
			int prevSwarmIndex = prevSwarmOfDim[curIndex];
			prevSwarm = oldSwarms[prevSwarmIndex];
			prevSwarmDims = prevSwarm.getDimIndices();
			prevGBest = prevSwarm.getBestPosition();

			for (int k = 0; k < prevSwarm.getNumDims(); k++) {
				if (prevSwarmDims[k] == curIndex) {
					prevIndex = k;
				}
			}
			newGBest[j] = prevGBest[prevIndex];

		}
		curSwarm.setGlobalBest(newGBest, 0); // index not needed
	}

	// Ring topology: Looks to immediate left and right and takes the best
	private double[] getLBest(Swarm s, int i) {
		double[] bestPos = null;
		double[] left = null;
		double[] middle = null;
		double[] right = null;

		if (i == 0) {
			left = s.getParticle(s.getSize() - 1).getCurrentPosition();
			right = s.getParticle(i + 1).getCurrentPosition();
		} else if (i == s.getSize() - 1) {
			left = s.getParticle(i - 1).getCurrentPosition();
			right = s.getParticle(0).getCurrentPosition();

		} else {
			left = s.getParticle(i - 1).getCurrentPosition();
			right = s.getParticle(i + 1).getCurrentPosition();
		}

		middle = s.getParticle(i).getCurrentPosition();

		double leftVal = fitness(s, contextVector, left);
		double midVal = fitness(s, contextVector, middle);
		double rightVal = fitness(s, contextVector, right);

		// System.out.println("left: "+leftVal+", mid: "+midVal+", right:
		// "+rightVal);

		// left is best
		if (leftVal <= midVal && leftVal <= rightVal) {
			bestPos = left;
		}
		// mid is best
		else if (midVal <= leftVal && midVal <= rightVal) {
			bestPos = middle;
		}
		// right is best
		else if (rightVal <= midVal && rightVal <= leftVal) {
			bestPos = right;
		}

		return bestPos;
	}

	private double cauchyDist() {

		Random r = new Random();
		double rand = r.nextDouble();

		return Math.tan(Math.PI * (rand - 0.5));
	}

	private void setRandomS() {
		Random rand = new Random();
		int x = rand.nextInt((swarmSizes.length - 1 - 0) + 1) + 0;

		while (swarmSizes[x] > dimensions) {
			x = rand.nextInt((swarmSizes.length - 1 - 0) + 1) + 0;
		}

		curS = swarmSizes[x];
		kParts = dimensions / curS;
		//System.out.println(dimensions + " / " + curS + " = " + kParts);
	}

	private void checkImproved() {
		double curFit = fn.compute(contextVector);
		if (curFit == lastFitness) {
			countSameFit += 1;
		}
	}

	private void updateSwarmOfDim() {
		for (int s = 0; s < swarms.length; s++) {
			for (int d = 0; d < swarms[s].getDimIndices().length; d++) {
				swarmOfDim[swarms[s].getIndex(d)] = s;
				// System.out.print("Swarm "+s+" looking after dim
				// "+swarms[s].getIndex(d)+", ");
			}
			// System.out.println();
		}
	}

}
