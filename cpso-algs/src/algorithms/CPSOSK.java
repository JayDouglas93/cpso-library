package algorithms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class CPSOSK extends Algorithm {

	private double[] contextVector;

	private Swarm[] swarms;
	private double c1, c2, inertia;
	private int kParts;

	public CPSOSK(HashMap<String, String> p) {

		super(p); // Sends HashMap to superclass who then creates global,
					// protected variables

		contextVector = new double[dimensions];

		inertia = Double.parseDouble(p.get("inertia"));
		c1 = Double.parseDouble(p.get("c1"));
		c2 = Double.parseDouble(p.get("c2"));
		kParts = Integer.parseInt(p.get("kParts"));

		fitPerEval = new double[fnEvals];

		initSwarms();
		begin();
	}

	public void begin() {

		int curFnEval = 0;

		while (!endCondition(curFnEval)) {
			if (logging) {
				System.out.println("Iteration " + curFnEval);

				for (int q = 0; q < dimensions; q++) {
					System.out.println("Dimension " + q + " is " + contextVector[q]);
				}
				System.out.println("...");
			}

			double[][] vectorEvals = new double[numParticles][dimensions];

			for (int i = 0; i < kParts; i++) {

				Swarm curSwarm = swarms[i];

				for (int j = 0; j < numParticles; j++) {
					Particle curParticle = curSwarm.getParticle(j);
					double[] curPos = curParticle.getCurrentPosition();
					double[] bestPos = curParticle.getBestPosition();

					if (fitness(curSwarm, contextVector, curPos) < fitness(curSwarm, contextVector, bestPos)) {

						curParticle.setBestPosition(curPos);
					}

					bestPos = curParticle.getBestPosition();

					double[] swarmBestPos = curSwarm.getBestPosition();
					double bestPosFit = fitness(curSwarm, contextVector, bestPos);

					if (bestPosFit < fitness(curSwarm, contextVector, swarmBestPos)) {

						curSwarm.setGlobalBest(bestPos, j);
						// Spot A for contextVector update

						double[] curSwarmBest = curSwarm.getBestPosition();

						if (bestPosFit < fn.compute(contextVector)) {

							// System.out.println(j+": bestPosFit:
							// "+bestPosFit+" < "+fn.compute(contextVector));

							for (int k = 0; k < curSwarm.getNumDims(); k++) {

								contextVector[curSwarm.getIndex(k)] = curSwarmBest[k];
							}
						}
					}

					// OLD FN EVAL UPDATE
					vectorEvals[j] = contextVector.clone();
					/*
					 * for (int k = 0; k < curSwarm.getNumDims(); k++) {
					 * vectorEvals[j][curSwarm.getIndex(k)] =
					 * contextVector[curSwarm.getIndex(k)];
					 * 
					 * System.out.print(j+": new best for dim "+curSwarm.
					 * getIndex(k)+": "+Math.round(100.0*contextVector[curSwarm.
					 * getIndex(k)])/100.0); } System.out.println();
					 */
				}
				// Spot B for contextVector update

				// OLD PARTICLE POS, VEL UPDATE
				for (int j = 0; j < numParticles; j++) {
					updateVelocity(swarms[i], j);
					updatePosition(swarms[i], j);
				}
			}

			for (int j = 0; j < numParticles; j++) {
				if (curFnEval < fnEvals) {
					/*
					 * System.out.print(j+": "); for (int x = 0 ; x <
					 * vectorEvals[j].length ; x++){
					 * System.out.print(Math.round(100.0*vectorEvals[j][x])/100.
					 * 0+" "); } System.out.print("\nCV: "); for (int x = 0 ; x
					 * < vectorEvals[j].length ; x++){
					 * System.out.print(Math.round(100.0*contextVector[x])/100.
					 * 0+" "); } System.out.println();
					 */
					fitPerEval[curFnEval] = fn.compute(vectorEvals[j]);
					// System.out.println("Fitness:
					// "+fitPerEval[curFnEval]+"\n");
					curFnEval += 1;

				}
			}

			// FEB 2018, POS VEL
		}
	}

	public void initSwarms() {

		swarms = new Swarm[kParts];

		int[] dimPerSwarm = initDimPerSwarm();

		for (int i = 0; i < kParts; i += 1) {
			swarms[i] = new Swarm(numParticles, dimPerSwarm[i], fn);
		}

		initDimensions();

		for (int i = 0; i < kParts; i += 1) {
			// Initialize context vector to random particles in each swarm
			Random rand = new Random();
			int index = rand.nextInt(numParticles);
			double[] swarmVal = swarms[i].getParticle(index).getCurrentPosition();

			for (int j = 0; j < dimPerSwarm[i]; j++) {
				contextVector[swarms[i].getIndex(j)] = swarmVal[j];
			}
		}

	}

	public void initDimensions() {

		int k1 = dimensions % kParts;
		int k2 = kParts - (dimensions % kParts);

		int dimK1 = (int) Math.ceil((double) dimensions / (double) kParts);
		int dimK2 = (int) Math.floor((double) dimensions / (double) kParts);

		int counter = 0;

		for (int i = 0; i < k1; i++) {
			for (int j = 0; j < dimK1; j++) {
				swarms[i].setIndex(j, counter);
				// System.out.print("S " + i + " --> " + swarms[i].getIndex(j) +
				// ", ");
				counter += 1;
			}
			// System.out.println();
		}

		for (int i = k1; i < kParts; i++) {
			for (int j = 0; j < dimK2; j++) {
				swarms[i].setIndex(j, counter);
				// System.out.print("S " + i + " --> " + swarms[i].getIndex(j) +
				// ", ");
				counter += 1;
			}
			// System.out.println();
		}

	}

	public int[] initDimPerSwarm() {

		int[] dimPerSwarm = new int[kParts];

		int k1 = dimensions % kParts;
		int k2 = kParts - (dimensions % kParts);

		int dimK1 = (int) (Math.ceil((double) dimensions / (double) kParts));
		int dimK2 = (int) Math.floor((double) dimensions / (double) kParts);

		// System.out.println(dimK1+" "+dimK2);

		for (int i = 0; i < k1; i++) {
			dimPerSwarm[i] = dimK1;
			// System.out.print(dimK1 + ", ");
		}

		for (int i = k1; i < kParts; i++) {
			dimPerSwarm[i] = dimK2;
			// System.out.print(dimK2 + ", ");
		}

		// System.out.println();

		return dimPerSwarm;
	}

	public void updateVelocity(Swarm s, int i) {
		Random r = new Random();

		// double inertia = 0.5 + r.nextDouble() / 2;
		double[] particleVelocity = s.getParticle(i).getVelocity();
		double[] particleBestPosition = s.getParticle(i).getBestPosition();
		double[] particlePosition = s.getParticle(i).getCurrentPosition();
		double[] gPosition = s.getBestPosition();

		double[] newVel = new double[s.getNumDims()];
		for (int j = 0; j < s.getNumDims(); j++) {
			double randDouble1 = r.nextDouble();
			double randDouble2 = r.nextDouble();
			newVel[j] = inertia * particleVelocity[j]
					+ c1 * randDouble1 * (particleBestPosition[j] - particlePosition[j])
					+ c2 * randDouble2 * (gPosition[j] - particlePosition[j]);
		}

		s.getParticle(i).setCurrentVelocity(newVel);

	}

	public void updatePosition(Swarm s, int i) {

		double[] particlePosition = s.getParticle(i).getCurrentPosition();
		double[] particleVelocity = s.getParticle(i).getVelocity();
		double[] newPos = new double[s.getNumDims()];

		for (int j = 0; j < s.getNumDims(); j++) {
			newPos[j] = particlePosition[j] + particleVelocity[j];
			double pMax = fn.getMax();
			double pMin = fn.getMin();

			if (newPos[j] > pMax) {
				newPos[j] = pMax;
			} else if (newPos[j] < pMin) {
				newPos[j] = pMin;
			}
		}
		s.getParticle(i).setCurrentPosition(newPos);

	}

}
