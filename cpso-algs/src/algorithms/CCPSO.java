package algorithms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class CCPSO extends Algorithm {

	private double[] contextVector;

	private Swarm[] swarms;
	private Swarm PSO;

	private double c1, c2, inertia;
	private int kParts;
	private int regroupRK;
	private int numLoopWeight;
	private boolean isVanilla;

	private int[] swarmOfDim;

	private int curFnEval;

	public CCPSO(HashMap<String, String> p) {

		super(p); // Sends HashMap to superclass who then creates global,
					// protected variables

		contextVector = new double[dimensions];
		inertia = Double.parseDouble(p.get("inertia"));
		c1 = Double.parseDouble(p.get("c1"));
		c2 = Double.parseDouble(p.get("c2"));
		kParts = Integer.parseInt(p.get("kParts"));
		regroupRK = Integer.parseInt(p.get("regroupEvery"));
		numLoopWeight = Integer.parseInt(p.get("numLoopWeight"));

		fitPerEval = new double[fnEvals];

		swarmOfDim = new int[dimensions];

		initSwarms();
		begin();
	}

	public void begin() {

		curFnEval = 0;
		int counter = 0;
		while (!endCondition(curFnEval)) {
			if (logging) {
				System.out.println("Iteration " + curFnEval);

				for (int q = 0; q < dimensions; q++) {
					System.out.println("Dimension " + q + " is " + contextVector[q]);
				}
				System.out.println("...");
			}

			if (counter == regroupRK) {

				randomRegroup();

				counter = 0;
			}

			loopCCPSO();

			initPSOSwarm();

			if (logging) {
				System.out.println("Starting PSO weight vector");
			}

			for (int i = 0; i < numLoopWeight; i++) {
				loopPSO();
			}
			if (logging) {
				System.out.println("Ending PSO weight vector");
			}

			// NEW PLACE FOR UPDATE OF CONTEXT VECTOR WITH NEW WEIGHT VECTOR

			double[] cloneCVector = contextVector.clone();

			for (int s = 0; s < kParts; s++) {
				Swarm curSwarm = swarms[s];
				double weight = PSO.getBestPosition()[s];

				for (int i = 0; i < curSwarm.getNumDims(); i++) {
					cloneCVector[curSwarm.getIndex(i)] *= weight;
				}
			}

			double newFit = fn.compute(cloneCVector);
			double oldFit = fn.compute(contextVector);

			if (newFit < oldFit) {
				contextVector = cloneCVector.clone();
				if (logging) {
					System.out.println("Old CV: " + oldFit + ", new CV: " + newFit);
					System.out.println("WEIGHTED VECTOR FOUND NEW CONTEXT VECTOR");
				}

				// Update personal bests of each subswarm with context vector
				for (int s = 0; s < kParts; s++) {
					Swarm curSwarm = swarms[s];
					for (int i = 0; i < curSwarm.getNumDims(); i++) {
						curSwarm.updateGlobalBest(i, contextVector[curSwarm.getIndex(i)]);
					}
				}

			}

			// OLD PLACE FOR REGROUPING

			counter += 1;
		}
	}

	private void loopCCPSO() {

		isVanilla = false;

		double[][] vectorEvals = new double[numParticles][dimensions];

		for (int i = 0; i < kParts; i++) {

			Swarm curSwarm = swarms[i];

			for (int j = 0; j < numParticles; j++) {

				Particle curParticle = curSwarm.getParticle(j);

				double[] curPos = curParticle.getCurrentPosition();
				double[] bestPos = curParticle.getBestPosition();

				if (fitness(curSwarm, contextVector, curPos) < fitness(curSwarm, contextVector, bestPos)) {
					curParticle.setBestPosition(curPos);
				}

				bestPos = curParticle.getBestPosition();
				double bestPosFit = fitness(curSwarm, contextVector, bestPos);

				double[] swarmBestPos = curSwarm.getBestPosition();

				if (bestPosFit < fitness(curSwarm, contextVector, swarmBestPos)) {
					curSwarm.setGlobalBest(bestPos, j);

					if (bestPosFit < fn.compute(contextVector)) {
						for (int k = 0; k < curSwarm.getNumDims(); k++) {
							// System.out.println("Replacing " +
							// contextVector[swarms[i].getIndex(k)] + " with " +
							// bestPos[k]);
							contextVector[curSwarm.getIndex(k)] = bestPos[k];
						}
					}
				}
				// OLD FN EVAL UPDATE
				vectorEvals[j] = contextVector.clone();
				/*
				 * for (int k = 0; k < curSwarm.getNumDims(); k++) {
				 * vectorEvals[j][curSwarm.getIndex(k)] =
				 * contextVector[curSwarm.getIndex(k)]; }
				 */
			}

			for (int j = 0; j < numParticles; j++) {
				updateVelocity(swarms[i], j);
				updatePosition(swarms[i], j);
			}
		}

		for (int j = 0; j < numParticles; j++) {
			if (curFnEval < fnEvals) {
				fitPerEval[curFnEval] = fn.compute(vectorEvals[j]);
				curFnEval += 1;
			}
		}

		// FEB 2018, POS VEL
	}

	private void loopPSO() {

		isVanilla = true;

		for (int j = 0; j < numParticles; j++) {
			Particle curParticle = PSO.getParticle(j);
			double[] curPos = curParticle.getCurrentPosition();
			double[] bestPos = curParticle.getBestPosition();

			if (fitnessPSO(curPos) < fitnessPSO(bestPos)) {

				curParticle.setBestPosition(curPos);
			}

			bestPos = curParticle.getBestPosition();

			double[] swarmBestPos = PSO.getBestPosition();
			if (fitnessPSO(bestPos) < fitnessPSO(swarmBestPos)) {
				PSO.setGlobalBest(bestPos, j);

				// Now take global best of PSO, compare it to old context
				// vector,
				// and update if improved

				// If you update the context vector every time,
				// you of course will find a better solution every evaluation

				// OLD SPOT FOR CONTEXT VECTOR UPDATE
			}

			// Even tho context vector is no longer updated here, will still
			// need to note it's fitness

			if (curFnEval < fnEvals) {
				double fnFitness = fn.compute(contextVector);

				fitPerEval[curFnEval] = fnFitness;

				curFnEval += 1;
			}
		}
		if (logging) {
			System.out.println("Final PSO best mulitplied by context vector...");
			for (int q = 0; q < kParts; q++) {
				System.out.println("Dimension " + q + " is " + PSO.getBestPosition()[q]);
			}
			System.out.println("...");
		}

		for (int j = 0; j < numParticles; j++) {
			updateVelocity(PSO, j);
			updatePosition(PSO, j);
		}
	}

	public void initSwarms() {

		swarms = new Swarm[kParts];
		int[] dimPerSwarm = initDimPerSwarm();

		for (int i = 0; i < kParts; i += 1) {
			swarms[i] = new Swarm(numParticles, dimPerSwarm[i], fn);
		}

		initDimensions();

		for (int i = 0; i < kParts; i += 1) {
			// Initialize context vector to random particles in each swarm
			Random rand = new Random();
			int index = rand.nextInt(numParticles);
			double[] swarmVal = swarms[i].getParticle(index).getCurrentPosition();

			for (int j = 0; j < dimPerSwarm[i]; j++) {
				contextVector[swarms[i].getIndex(j)] = swarmVal[j];
			}
		}

	}

	private void initPSOSwarm() {

		double pMin = (-1.0) * Double.MAX_VALUE;
		double pMax = Double.MAX_VALUE;
		double higher = 0;
		double lower = 0;

		for (int i = 0; i < contextVector.length; i++) {
			if (contextVector[i] >= 0) {
				lower = fn.getMin() / contextVector[i];
				higher = fn.getMax() / contextVector[i];
			} else {
				lower = fn.getMax() / contextVector[i];
				higher = fn.getMin() / contextVector[i];
			}

			if (lower > pMin) {
				pMin = lower;
			}
			if (higher < pMax) {
				pMax = higher;
			}
		}
		if (logging) {
			System.out.println("Interval of weight vector: (" + pMin + ", " + pMax + ")");
		}

		PSO = new Swarm(numParticles, kParts, pMin, pMax);

		// Don't need since each index references a subswarm
		/*
		 * for (int i = 0; i < dimensions; i++) { PSO.setIndex(i, i); }
		 */
	}

	public void initDimensions() {

		Random rnd = ThreadLocalRandom.current();
		ArrayList<Integer> nums = new ArrayList<Integer>();
		for (int i = 0; i < dimensions; i++) {
			nums.add(i);
		}

		int k1 = dimensions % kParts;
		int k2 = kParts - (dimensions % kParts);

		int dimK1 = (int) Math.ceil((double) dimensions / (double) kParts);
		int dimK2 = (int) Math.floor((double) dimensions / (double) kParts);

		for (int i = 0; i < k1; i++) {
			for (int j = 0; j < dimK1; j++) {

				int whichIndex = nums.remove(rnd.nextInt(nums.size()));

				swarms[i].setIndex(j, whichIndex);
				//System.out.print("S " + i + " --> " + swarms[i].getIndex(j) + ", ");
				swarmOfDim[whichIndex] = i;
			}
			//System.out.println();
		}

		for (int i = k1; i < kParts; i++) {
			for (int j = 0; j < dimK2; j++) {

				int whichIndex = nums.remove(rnd.nextInt(nums.size()));

				swarms[i].setIndex(j, whichIndex);
				//System.out.print("S " + i + " --> " + swarms[i].getIndex(j) + ", ");
				swarmOfDim[whichIndex] = i;
			}
			//System.out.println();
		}

	}

	public int[] initDimPerSwarm() {

		int[] dimPerSwarm = new int[kParts];

		int k1 = dimensions % kParts;
		int k2 = kParts - (dimensions % kParts);

		int dimK1 = (int) (Math.ceil((double) dimensions / (double) kParts));
		int dimK2 = (int) Math.floor((double) dimensions / (double) kParts);

		//System.out.println(dimK1 + " " + dimK2);

		for (int i = 0; i < k1; i++) {
			dimPerSwarm[i] = dimK1;
			//System.out.print(dimK1 + ", ");
		}

		for (int i = k1; i < kParts; i++) {
			dimPerSwarm[i] = dimK2;
			//System.out.print(dimK2 + ", ");
		}

		//System.out.println();

		return dimPerSwarm;
	}

	public void updateVelocity(Swarm s, int i) {
		Random r = new Random();

		double[] particleVelocity = s.getParticle(i).getVelocity();
		double[] particleBestPosition = s.getParticle(i).getBestPosition();
		double[] particlePosition = s.getParticle(i).getCurrentPosition();
		double[] gPosition = s.getBestPosition();

		int dims = 0;

		if (isVanilla) {
			dims = kParts;
		} else {
			dims = s.getNumDims();
		}

		double[] newVel = new double[dims];

		for (int j = 0; j < dims; j++) {
			double randDouble1 = r.nextDouble();
			double randDouble2 = r.nextDouble();
			newVel[j] = inertia * particleVelocity[j]
					+ c1 * randDouble1 * (particleBestPosition[j] - particlePosition[j])
					+ c2 * randDouble2 * (gPosition[j] - particlePosition[j]);
		}

		s.getParticle(i).setCurrentVelocity(newVel);

	}

	public void updatePosition(Swarm s, int i) {

		double[] particlePosition = s.getParticle(i).getCurrentPosition();
		double[] particleVelocity = s.getParticle(i).getVelocity();

		int dims = 0;

		if (isVanilla) {
			dims = kParts;
		} else {
			dims = s.getNumDims();
		}

		double[] newPos = new double[dims];

		for (int j = 0; j < dims; j++) {
			newPos[j] = particlePosition[j] + particleVelocity[j];
			double pMax = s.getPMax();
			double pMin = s.getPMin();

			if (newPos[j] > pMax) {
				newPos[j] = pMax;
			} else if (newPos[j] < pMin) {
				newPos[j] = pMin;
			}
		}
		s.getParticle(i).setCurrentPosition(newPos);

	}

	public Swarm[] copySwarms() {

		Swarm[] copySwarms = new Swarm[swarms.length];
		for (int s = 0; s < swarms.length; s++) {

			Swarm swrm = swarms[s];

			copySwarms[s] = new Swarm(swrm);

		}
		return copySwarms;
	}

	/**
	 * Used randomly reshuffle the dimensions amongst the sub-swarms
	 */
	public void randomRegroup() {

		//System.out.println("Regrouping");

		int[] prevSwarmOfDim = swarmOfDim.clone();
		Swarm[] oldSwarms = copySwarms();

		initDimensions();

		for (int i = 0; i < kParts; i++) {

			updatePBests(i, prevSwarmOfDim, oldSwarms);
			updateGlobalBests(i, prevSwarmOfDim, oldSwarms);
			// The swarm will look to new indicies in the context vector now,
			// so...
			// there is no need to update the values since the appropriate
			// values
			// will be changed anyways when they look at new indicies
		}

	}

	public void updatePBests(int i, int[] prevSwarmOfDim, Swarm[] oldSwarms) {
		Swarm curSwarm = swarms[i];
		int[] curSwarmDims = curSwarm.getDimIndices();

		Swarm prevSwarm;
		int[] prevSwarmDims;
		double[] prevSwarmPBest;

		for (int p = 0; p < numParticles; p++) {
			double[] newPBest = new double[curSwarm.getNumDims()];
			for (int j = 0; j < curSwarm.getNumDims(); j++) {
				int curIndex = curSwarmDims[j];
				int prevIndex = 0;
				int prevSwarmIndex = prevSwarmOfDim[curIndex];

				// previous swarm looking at the current dimension at index j
				prevSwarm = oldSwarms[prevSwarmIndex];

				// error: using updated dim indices
				prevSwarmDims = prevSwarm.getDimIndices();
				prevSwarmPBest = prevSwarm.getParticle(p).getBestPosition();
				for (int k = 0; k < prevSwarm.getNumDims(); k++) {

					if (prevSwarmDims[k] == curIndex) {
						prevIndex = k;
					}
				}
				newPBest[j] = prevSwarmPBest[prevIndex];

			}
			curSwarm.getParticle(p).setBestPosition(newPBest);
		}
	}

	public void updateGlobalBests(int i, int[] prevSwarmOfDim, Swarm[] oldSwarms) {

		Swarm curSwarm = swarms[i];
		int[] curSwarmDims = curSwarm.getDimIndices();

		Swarm prevSwarm;
		int[] prevSwarmDims;

		double[] prevGBest;

		double[] newGBest = new double[curSwarm.getNumDims()];

		for (int j = 0; j < curSwarm.getNumDims(); j++) {
			int curIndex = curSwarmDims[j];
			int prevIndex = 0;
			int prevSwarmIndex = prevSwarmOfDim[curIndex];
			prevSwarm = oldSwarms[prevSwarmIndex];
			prevSwarmDims = prevSwarm.getDimIndices();
			prevGBest = prevSwarm.getBestPosition();

			for (int k = 0; k < oldSwarms[prevSwarmIndex].getNumDims(); k++) {
				if (prevSwarmDims[k] == curIndex) {
					prevIndex = k;
				}
			}
			newGBest[j] = prevGBest[prevIndex];

		}
		curSwarm.setGlobalBest(newGBest, 0); // index not needed
	}

	// Eval a clone of the CV with the new weights of each subswarm multiplied
	// by their respective dimensions
	private double fitnessPSO(double[] newWeights) {

		double[] cloneCVector = contextVector.clone();

		for (int s = 0; s < kParts; s++) {
			Swarm curSwarm = swarms[s];
			double weight = newWeights[s];

			for (int i = 0; i < curSwarm.getNumDims(); i++) {
				cloneCVector[curSwarm.getIndex(i)] *= weight; // Multiply itself
																// by the weight
			}
		}

		double fnVal = fn.compute(cloneCVector);
		return fnVal;
	}

}
