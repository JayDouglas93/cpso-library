package algorithms;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class AMCCPSO extends Algorithm {

	private double[][] contextVectors;
	private int curCV;

	private Swarm[] swarms;
	private SwarmManager swarmManager;

	private double cauchyProb;
	private int kParts;
	private int curS;
	private int regroupEvery;

	private int numSPSC;

	private int[] swarmOfDim;

	private int[] swarmSizes;

	private double lastFitness;
	private int countSameFit; // What's current # of stagnated iterations
	private int maxSameFit; // How many stagnated iterations until you act on it

	private int curFnEval;

	private double p1;
	private double p2;

	public AMCCPSO(HashMap<String, String> p) {

		super(p); // Sends HashMap to superclass who then creates global,
					// protected variables

		int numCVs = Integer.parseInt(p.get("numContextVectors"));
		contextVectors = new double[numCVs][dimensions];

		// We want to start off with a D-dimensional PSO to selection 'p' best
		// particle positions for CVs
		kParts = 1;
		regroupEvery = Integer.parseInt(p.get("regroupEvery"));
		numSPSC = Integer.parseInt(p.get("numSPSC"));
		p1 = Double.parseDouble(p.get("probGlobalBest"));
		p2 = Double.parseDouble(p.get("probSubSwarmBest"));

		cauchyProb = Double.parseDouble(p.get("cauchyProb"));

		fitPerEval = new double[fnEvals];

		swarmOfDim = new int[dimensions];

		String[] temp = p.get("swarmSizes").split(",");
		swarmSizes = new int[temp.length];
		for (int s = 0; s < temp.length; s++) {
			swarmSizes[s] = Integer.parseInt(temp[s].trim());
		}

		maxSameFit = Integer.parseInt(p.get("maxSameFit"));
		countSameFit = 0;

		int personalGlobalBest = Integer.parseInt(p.get("pgBestParamM"));
		swarmManager = new SwarmManager(personalGlobalBest, numParticles, fn);

		lastFitness = Double.MAX_VALUE;

		initSwarms();
		begin();
	}

	public void begin() {

		curFnEval = 0;
		int counter = 0;
		while (!endCondition(curFnEval)) {

			if (logging) {
				System.out.println("Iteration " + curFnEval);
			}

			// CHECK LAST FITNESS, RESTRUCTURE IF NO IMPROVEMENT
			// POSITIONS ARE RANDOMLY RESET FOR NEW SUBSWARMS
			checkImproved();
			lastFitness = fn.compute(contextVectors[curCV]);
			if (countSameFit == maxSameFit) { // set random s
				if (logging) {
					System.out.println("Fitness stagnated for " + countSameFit + " iterations, regrouping...");
				}
				setRandomS(); // kParts will be updated with new random s

				// Used to determine new structure
				int k1 = dimensions % kParts;
				int s1 = (int) Math.ceil((double) dimensions / (double) kParts);
				int s2 = (int) Math.floor((double) dimensions / (double) kParts);

				int[] dimPerSwarm = new int[kParts];

				for (int s = 0; s < kParts; s++) {
					if (s < k1) {
						dimPerSwarm[s] = s1;
					} else {
						dimPerSwarm[s] = s2;
					}
				}

				swarms = swarmManager.changeK(swarms, dimensions, dimPerSwarm);
				countSameFit = 0;
				updateSwarmOfDim();
				
				randomRegroup();
				
			}

			// RANDOMLY REGROUP DIMENSIONS OF EACH SUBSWARM
			//if (counter == regroupEvery) {

			//	randomRegroup();

				//counter = 0;
			//}

			loopAMCCPSO();

			counter += 1;
		}

	}

	private void loopAMCCPSO() {

		double[][] vectorEvals = new double[numParticles][dimensions];
		int count = 0;

		for (int i = 0; i < kParts; i++) {

			Swarm curSwarm = swarms[i];

			// Get list of probabilities for each CV before looping through each
			// particle
			double[] CVProbs = getCVProbs();

			for (int j = 0; j < numParticles; j++) {

				// Randomly select CV index based on probability
				int partCVIndex = getRandCV(CVProbs);
				curCV = partCVIndex;

				Particle curParticle = curSwarm.getParticle(j);

				double[] curPos = curParticle.getCurrentPosition();
				double[] bestPos = curParticle.getBestPosition();

				if (fitness(curSwarm, contextVectors[curCV], curPos) < fitness(curSwarm, contextVectors[curCV],
						bestPos)) {
					curParticle.setBestPosition(curPos);
				}

				bestPos = curParticle.getBestPosition();
				double bestPosFit = fitness(curSwarm, contextVectors[curCV], bestPos);

				// You still need swarm best
				double[] swarmBestPos = curSwarm.getBestPosition();
				if (bestPosFit < fitness(curSwarm, contextVectors[curCV], swarmBestPos)) {
					curSwarm.setGlobalBest(bestPos, j);
				}

				if (bestPosFit < fn.compute(contextVectors[curCV])) {

					for (int k = 0; k < curSwarm.getNumDims(); k++) {

						contextVectors[curCV][curSwarm.getIndex(k)] = bestPos[k];
					}
				}

				int bestCV = getBestCV();

				vectorEvals[j] = contextVectors[bestCV].clone();
				/*
				 * for (int k = 0; k < curSwarm.getNumDims(); k++) {
				 * vectorEvals[j][curSwarm.getIndex(k)] =
				 * contextVectors[curCV][curSwarm.getIndex(k)]; }
				 */
			}

			int[][] SPSC = getSPSC(curSwarm);

			for (int x = 0; x < SPSC.length; x++) {
				for (int y = 0; y < SPSC[x].length; y++) {

					updatePosition(swarms[i], SPSC, x, y);
				}
			}

			for (int j = 0; j < numParticles; j++) {
				updatePosition(swarms[i], j);
			}

			// Once in every 50 iterations, update worst CV...

			count += 1;

			if (count == 50) {
				updateWorstCV();
				count = 0;
			}

		}

		/*
		 * if (curFnEval < fnEvals){ double fnFitness =
		 * fn.compute(contextVector);
		 * 
		 * fitPerEval[curFnEval] = fnFitness;
		 * 
		 * curFnEval += 1; }
		 */

		for (int j = 0; j < numParticles; j++) {
			if (curFnEval < fnEvals) {
				fitPerEval[curFnEval] = fn.compute(vectorEvals[j]);
				curFnEval += 1;
			}
		}

		// FEB 2018, POS VEL
	}

	public void initSwarms() {

		swarms = new Swarm[kParts];
		int[] dimPerSwarm = initDimPerSwarm();

		for (int i = 0; i < kParts; i += 1) {
			swarms[i] = new Swarm(numParticles, dimPerSwarm[i], fn);
		}

		initDimensions();

		for (int i = 0; i < kParts; i += 1) {
			// Initialize context vector to best 'p' particles

			// There will only be one swarm of D-dimensions
			Swarm curSwarm = swarms[i];
			ArrayList<double[]> bestParticles = new ArrayList<double[]>();

			for (int p = 0; p < curSwarm.getSize(); p++) {

				double[] partPos = curSwarm.getParticle(p).getCurrentPosition();
				double partFit = fn.compute(partPos);
				// Sorted insert based on fitness

				int index = 0;

				// While not at end and inserting particle is worse than current
				// particle
				while (index < bestParticles.size() && partFit > fn.compute(bestParticles.get(index))) {
					index += 1;
				}
				// here, your index is here the end of the list or your particle
				// is better than cur particle i
				// so place at i and shift it down
				bestParticles.add(index, partPos);
			}

			for (int p = 0; p < contextVectors.length; p++) {
				contextVectors[p] = bestParticles.get(p);
			}
		}

		// lastFitness = fn.compute(contextVector);

	}

	public void initDimensions() {

		Random rnd = ThreadLocalRandom.current();
		ArrayList<Integer> nums = new ArrayList<Integer>();
		for (int i = 0; i < dimensions; i++) {
			nums.add(i);
		}

		int k1 = dimensions % kParts;
		int k2 = kParts - (dimensions % kParts);

		int dimK1 = (int) Math.ceil((double) dimensions / (double) kParts);
		int dimK2 = (int) Math.floor((double) dimensions / (double) kParts);

		for (int i = 0; i < k1; i++) {
			for (int j = 0; j < dimK1; j++) {

				int whichIndex = nums.remove(rnd.nextInt(nums.size()));

				swarms[i].setIndex(j, whichIndex);
				if (logging) {
					System.out.print("S " + i + " --> " + swarms[i].getIndex(j) + ", ");
				}
				swarmOfDim[whichIndex] = i;
			}
			// System.out.println();
		}

		for (int i = k1; i < kParts; i++) {
			for (int j = 0; j < dimK2; j++) {

				int whichIndex = nums.remove(rnd.nextInt(nums.size()));

				swarms[i].setIndex(j, whichIndex);
				if (logging) {
					System.out.print("S " + i + " --> " + swarms[i].getIndex(j) + ", ");
				}
				swarmOfDim[whichIndex] = i;
			}
			// System.out.println();
		}
		if (logging) {
			System.out.println("\n");
		}
	}

	public int[] initDimPerSwarm() {

		int[] dimPerSwarm = new int[kParts];

		int k1 = dimensions % kParts;
		int k2 = kParts - (dimensions % kParts);

		int dimK1 = (int) (Math.ceil((double) dimensions / (double) kParts));
		int dimK2 = (int) Math.floor((double) dimensions / (double) kParts);

		if (logging) {
			System.out.println(dimK1 + " " + dimK2);
		}

		for (int i = 0; i < k1; i++) {
			dimPerSwarm[i] = dimK1;
			if (logging) {
				System.out.print(dimK1 + ", ");
			}
		}

		for (int i = k1; i < kParts; i++) {
			dimPerSwarm[i] = dimK2;
			if (logging) {
				System.out.print(dimK2 + ", ");
			}
		}

		if (logging) {
			System.out.println();
		}

		return dimPerSwarm;
	}

	public void updatePosition(Swarm s, int[][] SPSC, int x, int y) {

		int partIndex = SPSC[x][y];

		double[] partBestPos = s.getParticle(partIndex).getBestPosition().clone();

		int dims = 0;
		dims = s.getNumDims();

		double[] newPos = new double[dims];

		Random r = new Random();
		double rand = r.nextDouble(); // (0,1)
		double[] randPos = new double[dims];

		if (rand <= p1) {

			// Extract global a sub-section of the "global best"
			int bestCV = getBestCV();
			double[] bestCVVector = contextVectors[bestCV].clone();

			for (int d = 0; d < dims; d++) {
				randPos[d] = bestCVVector[s.getIndex(d)];
			}

		} else if (p1 < rand && rand <= p2) {

			// "Best particle of jth sub-component", AKA, sub-swarm best
			randPos = s.getBestPosition().clone();
		} else {

			// Will find best particle in current, left, and right SPSC
			randPos = getLBest(s, SPSC, x, y).clone();
		}

		double coef = r.nextGaussian();

		for (int j = 0; j < dims; j++) {
			newPos[j] = 0.5 * (partBestPos[j] + randPos[j]) + coef * (Math.abs(partBestPos[j] - randPos[j]));
			double pMax = s.getPMax();
			double pMin = s.getPMin();

			if (newPos[j] > pMax) {
				newPos[j] = pMax;
			} else if (newPos[j] < pMin) {
				newPos[j] = pMin;
			}
		}
		s.getParticle(partIndex).setCurrentPosition(newPos);

	}

	public Swarm[] copySwarms() {

		Swarm[] copySwarms = new Swarm[swarms.length];
		for (int s = 0; s < swarms.length; s++) {

			Swarm swrm = swarms[s];

			copySwarms[s] = new Swarm(swrm);

		}
		return copySwarms;
	}

	/**
	 * Used randomly reshuffle the dimensions amongst the sub-swarms
	 */
	public void randomRegroup() {

		if (logging) {
			System.out.println("Regrouping");
		}
		int[] prevSwarmOfDim = swarmOfDim.clone();
		Swarm[] oldSwarms = copySwarms();

		initDimensions();

		for (int i = 0; i < kParts; i++) {

			updatePBests(i, prevSwarmOfDim, oldSwarms);
			updateGlobalBests(i, prevSwarmOfDim, oldSwarms);
			// The swarm will look to new indicies in the context vector now,
			// so...
			// there is no need to update the values since the appropriate
			// values
			// will be changed anyways
		}

	}

	public void updatePBests(int i, int[] prevSwarmOfDim, Swarm[] oldSwarms) {
		Swarm curSwarm = swarms[i];
		int[] curSwarmDims = curSwarm.getDimIndices();

		Swarm prevSwarm;
		int[] prevSwarmDims;
		double[] prevSwarmPBest;

		for (int p = 0; p < numParticles; p++) {
			double[] newPBest = new double[curSwarm.getNumDims()];
			for (int j = 0; j < curSwarm.getNumDims(); j++) {
				int curIndex = curSwarmDims[j];
				int prevIndex = 0;
				int prevSwarmIndex = prevSwarmOfDim[curIndex];

				// previous swarm looking at the current dimension at index j
				// System.out.println("legnth of old swarms:
				// "+oldSwarms.length);
				prevSwarm = oldSwarms[prevSwarmIndex];

				// error: using updated dim indices
				prevSwarmDims = prevSwarm.getDimIndices();
				prevSwarmPBest = prevSwarm.getParticle(p).getBestPosition();
				for (int k = 0; k < prevSwarm.getNumDims(); k++) {

					if (prevSwarmDims[k] == curIndex) {
						prevIndex = k;
					}
				}
				newPBest[j] = prevSwarmPBest[prevIndex];

			}
			curSwarm.getParticle(p).setBestPosition(newPBest);
		}
	}

	public void updateGlobalBests(int i, int[] prevSwarmOfDim, Swarm[] oldSwarms) {

		Swarm curSwarm = swarms[i];
		int[] curSwarmDims = curSwarm.getDimIndices();

		Swarm prevSwarm;
		int[] prevSwarmDims;

		double[] prevGBest;

		double[] newGBest = new double[curSwarm.getNumDims()];

		for (int j = 0; j < curSwarm.getNumDims(); j++) {
			int curIndex = curSwarmDims[j];
			int prevIndex = 0;
			int prevSwarmIndex = prevSwarmOfDim[curIndex];
			prevSwarm = oldSwarms[prevSwarmIndex];
			prevSwarmDims = prevSwarm.getDimIndices();
			prevGBest = prevSwarm.getBestPosition();

			for (int k = 0; k < prevSwarm.getNumDims(); k++) {
				if (prevSwarmDims[k] == curIndex) {
					prevIndex = k;
				}
			}
			newGBest[j] = prevGBest[prevIndex];

		}
		curSwarm.setGlobalBest(newGBest, 0); // index not needed
	}

	// Ring topology: Looks to immediate left and right SPSC, find each SPSC's
	// best, and then return the best of the 3
	private double[] getLBest(Swarm s, int[][] SPSC, int x, int y) {

		double[] bestPos = null;
		int[] left = null;
		int[] middle = null;
		int[] right = null;

		if (x == 0) {
			left = SPSC[SPSC.length - 1];
			right = SPSC[x + 1];

		} else if (x == SPSC.length - 1) {
			left = SPSC[x - 1];
			right = SPSC[0];

		} else {
			left = SPSC[x - 1];
			right = SPSC[x + 1];
		}

		middle = SPSC[x];

		// MARCH 28: NOW FIND BEST PARTICLE IN left, middle, right
		double[] leftPos = getBestSPSC(s, left);
		double[] middlePos = getBestSPSC(s, middle);
		double[] rightPos = getBestSPSC(s, right);

		double leftVal = fitness(s, contextVectors[curCV], leftPos);
		double midVal = fitness(s, contextVectors[curCV], middlePos);
		double rightVal = fitness(s, contextVectors[curCV], rightPos);

		// System.out.println("left: "+leftVal+", mid: "+midVal+", right:
		// "+rightVal);

		// left is best
		if (leftVal <= midVal && leftVal <= rightVal) {
			bestPos = leftPos;
		}
		// mid is best
		else if (midVal <= leftVal && midVal <= rightVal) {
			bestPos = middlePos;
		}
		// right is best
		else if (rightVal <= midVal && rightVal <= leftVal) {
			bestPos = rightPos;
		}

		return bestPos;
	}

	private void setRandomS() {
		Random rand = new Random();
		int x = rand.nextInt((swarmSizes.length - 1 - 0) + 1) + 0;

		while (swarmSizes[x] > dimensions) {
			x = rand.nextInt((swarmSizes.length - 1 - 0) + 1) + 0;
		}

		curS = swarmSizes[x];
		kParts = dimensions / curS;
		//System.out.println(dimensions + " / " + curS + " = " + kParts);
	}

	private void checkImproved() {
		double curFit = fn.compute(contextVectors[curCV]);
		if (curFit == lastFitness) {
			countSameFit += 1;
		}
	}

	private void updateSwarmOfDim() {
		for (int s = 0; s < swarms.length; s++) {
			for (int d = 0; d < swarms[s].getDimIndices().length; d++) {
				swarmOfDim[swarms[s].getIndex(d)] = s;
			}

		}
	}

	private double probCV(int whichCV) {

		double result;

		double threshold = 1.0 * Math.pow(10.0, -50.0);
		double curCVFit = fn.compute(contextVectors[whichCV]);

		if (curCVFit >= threshold) {

			double num = 1.0 / curCVFit;

			double denom = 0;

			for (int i = 0; i < contextVectors.length; i++) {
				double val = 1.0 / fn.compute(contextVectors[i]);
				denom += val;
			}

			result = num / denom;
		} else {

			result = 1.0 / threshold;
		}
		return result;
	}

	private int getBestCV() {

		// Looking for lowest value
		double bestFit = fn.compute(contextVectors[0]);
		int bestIndex = 0;

		for (int c = 0; c < contextVectors.length; c++) {

			double curFit = fn.compute(contextVectors[c]);

			if (curFit < bestFit) {
				bestFit = curFit;
				bestIndex = c;
			}
		}

		return bestIndex;

	}

	private int getWorstCV() {

		// Looking for highest value
		double bestFit = fn.compute(contextVectors[0]);
		int bestIndex = 0;

		for (int c = 0; c < contextVectors.length; c++) {

			double curFit = fn.compute(contextVectors[c]);

			if (curFit > bestFit) {
				bestFit = curFit;
				bestIndex = c;
			}
		}

		return bestIndex;

	}

	private double[] getCVProbs() {

		double[] probs = new double[contextVectors.length];

		for (int cv = 0; cv < contextVectors.length; cv++) {
			probs[cv] = probCV(cv);
			// System.out.println(probs[cv]+" ");
		}
		// System.out.println();
		return probs;
	}

	// Randomly, based on each CVs probability
	private int getRandCV(double[] cvProbs) {

		double p = Math.random();
		double cumulativeProbability = 0.0;

		for (int cv = 0; cv < cvProbs.length; cv++) {
			cumulativeProbability += cvProbs[cv];
			if (p <= cumulativeProbability) {
				return cv;
			}
		}
		return -1;
	}

	private int[][] getSPSC(Swarm s) {

		int[][] newSPSC = new int[numSPSC][numParticles / numSPSC];

		Random rnd = ThreadLocalRandom.current();
		ArrayList<Integer> nums = new ArrayList<Integer>();
		for (int i = 0; i < numParticles; i++) {
			nums.add(i);
		}

		for (int i = 0; i < newSPSC.length; i++) {

			// System.out.print("SPSC "+i+": ");

			for (int j = 0; j < newSPSC[0].length; j++) {

				int randIndex = nums.remove(rnd.nextInt(nums.size()));

				// System.out.print(randIndex+" ");

				newSPSC[i][j] = randIndex;

			}
			// System.out.println();
		}
		return newSPSC;
	}

	private double[] getBestSPSC(Swarm s, int[] SPSC) {

		double[] bestPos = null;
		double bestFit = Double.MAX_VALUE;

		for (int i = 0; i < SPSC.length; i++) {

			double[] curPos = s.getParticle(SPSC[i]).getCurrentPosition();
			double curFit = fitness(s, contextVectors[curCV], curPos);

			if (curFit < bestFit) {
				bestPos = curPos;
				bestFit = curFit;
			}
		}

		return bestPos;
	}

	private void updateWorstCV() {

		int bestIndex = getBestCV();
		int worstIndex = getWorstCV();

		double[] bestCV = contextVectors[bestIndex].clone();
		double[] worstCV = contextVectors[worstIndex].clone();

		double[] newCV = new double[bestCV.length];

		Random r = new Random();

		for (int i = 0; i < bestCV.length; i++) {

			if (r.nextDouble() < 0.5) {
				newCV[i] = bestCV[i];
			} else {
				newCV[i] = worstCV[i];
			}

		}

		if (fn.compute(newCV) < fn.compute(worstCV)) {
			contextVectors[worstIndex] = newCV;
		}
	}

	@Override
	public void updatePosition(Swarm s, int i) {
		// TODO Auto-generated method stub

	}
}
