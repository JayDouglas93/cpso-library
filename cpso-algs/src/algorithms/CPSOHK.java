package algorithms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class CPSOHK extends Algorithm {

	private double[] contextVector;

	private Swarm[] CPSOSK;
	private Swarm PSO;

	private double c1, c2, inertia;
	private int kParts;
	private boolean isVanilla;

	private int curFnEval;

	public CPSOHK(HashMap<String, String> p) {

		super(p); // Sends HashMap to superclass who then creates global,
					// protected variables

		contextVector = new double[dimensions];

		inertia = Double.parseDouble(p.get("inertia"));
		c1 = Double.parseDouble(p.get("c1"));
		c2 = Double.parseDouble(p.get("c2"));
		kParts = Integer.parseInt(p.get("kParts"));

		fitPerEval = new double[fnEvals];

		initSwarms();
		initPSOSwarm();
		begin();
	}

	public void begin() {

		curFnEval = 0;
		while (!endCondition(curFnEval)) {

			if (logging) {
				System.out.println("PSO Iteration " + curFnEval);

				for (int q = 0; q < dimensions; q++) {
					System.out.println("Dimension " + q + " is " + PSO.getBestPosition()[q]);
				}
				System.out.println("...");

				System.out.println("Looping CPSOSK");
			}

			loopCPSOSK();

			// Context vector of CPSOSK overwriting random particle in PSO
			Random rand = new Random();
			int k = 1 + (int) (((numParticles / 2) - 1) * rand.nextDouble());

			while (k == PSO.getBpIndex()) {
				k = 1 + (int) (((numParticles / 2) - 1) * rand.nextDouble());
			}
			PSO.setParticlePos(k, contextVector.clone());

			if (logging) {
				System.out.println("CPSO Iteration " + curFnEval);

				for (int q = 0; q < dimensions; q++) {
					System.out.println("Dimension " + q + " is " + contextVector[q]);
				}
				System.out.println("...");

				System.out.println("Looping PSO");
			}

			loopPSO();

			// Overwriting random particles in each subswarm with
			int dimCount = 0;
			double[] newPos;
			for (int j = 0; j < kParts; j++) {
				Swarm curSwarm = CPSOSK[j];
				int numDims = curSwarm.getDimIndices().length;
				rand = new Random();
				k = 1 + (int) (((numParticles / 2) - 1) * rand.nextDouble());

				while (k == curSwarm.getBpIndex()) {
					k = 1 + (int) (((numParticles / 2) - 1) * rand.nextDouble());
				}
				newPos = new double[numDims];
				for (int i = 0; i < numDims; i++) {
					newPos[i] = PSO.getBestPosition()[dimCount];
					dimCount += 1;
				}
				curSwarm.setParticlePos(k, newPos);

			}

		}

	}

	private void loopCPSOSK() {

		isVanilla = false;

		double[][] vectorEvals = new double[numParticles][dimensions];

		for (int i = 0; i < kParts; i++) {

			Swarm curSwarm = CPSOSK[i];

			for (int j = 0; j < numParticles; j++) {
				Particle curParticle = curSwarm.getParticle(j);
				double[] curPos = curParticle.getCurrentPosition();
				double[] bestPos = curParticle.getBestPosition();

				if (fitness(curSwarm, contextVector, curPos) < fitness(curSwarm, contextVector, bestPos)) {

					curParticle.setBestPosition(curPos);
				}

				bestPos = curParticle.getBestPosition();
				double bestPosFit = fitness(curSwarm, contextVector, bestPos);

				double[] swarmBestPos = curSwarm.getBestPosition();

				if (bestPosFit < fitness(curSwarm, contextVector, swarmBestPos)) {

					curSwarm.setGlobalBest(bestPos, j);

					// Update context vector with new global best
					if (bestPosFit < fn.compute(contextVector)) {
						for (int k = 0; k < curSwarm.getNumDims(); k++) {
							contextVector[curSwarm.getIndex(k)] = bestPos[k];
						}
					}
				}

				// OLD FN EVAL UPDATE
				vectorEvals[j] = contextVector.clone();
				/*
				 * for (int k = 0; k < curSwarm.getNumDims(); k++) {
				 * vectorEvals[j][curSwarm.getIndex(k)] =
				 * contextVector[curSwarm.getIndex(k)]; }
				 */
			}
			// OLD POS VEL UPDATE
			for (int j = 0; j < numParticles; j++) {
				updateVelocity(CPSOSK[i], j);
				updatePosition(CPSOSK[i], j);
			}
		}

		for (int j = 0; j < numParticles; j++) {
			if (curFnEval < fnEvals) {
				fitPerEval[curFnEval] = fn.compute(vectorEvals[j]);
				curFnEval += 1;
			}
		}

		// FEB 2018, POS VEL
	}

	private void loopPSO() {

		isVanilla = true;

		for (int j = 0; j < numParticles; j++) {
			Particle curParticle = PSO.getParticle(j);
			double[] curPos = curParticle.getCurrentPosition();
			double[] bestPos = curParticle.getBestPosition();

			if (fitnessPSO(curPos) < fitnessPSO(bestPos)) {

				curParticle.setBestPosition(curPos);
			}

			bestPos = curParticle.getBestPosition();

			double[] swarmBestPos = PSO.getBestPosition();
			if (fitnessPSO(bestPos) < fitnessPSO(swarmBestPos)) {
				PSO.setGlobalBest(bestPos, j);

				double[] newSwarmBest = PSO.getBestPosition();

				if (fn.compute(newSwarmBest) < fn.compute(contextVector)) {
					contextVector = newSwarmBest;
				}
			}

			if (curFnEval < fnEvals) {
				double fnFitness = fn.compute(contextVector);

				fitPerEval[curFnEval] = fnFitness;

				curFnEval += 1;
			}

		}

		for (int j = 0; j < numParticles; j++) {
			updateVelocity(PSO, j);
			updatePosition(PSO, j);
		}
	}

	public void initSwarms() {

		CPSOSK = new Swarm[kParts];
		int[] dimPerSwarm = initDimPerSwarm();

		for (int i = 0; i < kParts; i += 1) {
			CPSOSK[i] = new Swarm(numParticles, dimPerSwarm[i], fn);
		}

		initDimensions();

		for (int i = 0; i < kParts; i += 1) {
			// Initialize context vector to random particles in each swarm
			Random rand = new Random();
			int index = rand.nextInt(numParticles);
			Particle bestP = CPSOSK[i].getParticle(index);
			bestP.setBest(true);
			double[] swarmVal = bestP.getCurrentPosition();

			for (int j = 0; j < CPSOSK[i].getNumDims(); j++) {
				contextVector[CPSOSK[i].getIndex(j)] = swarmVal[j];
			}
		}

	}

	private void initPSOSwarm() {

		PSO = new Swarm(numParticles, dimensions, fn);

		for (int i = 0; i < dimensions; i++) {
			PSO.setIndex(i, i);
		}
	}

	public void initDimensions() {

		int k1 = dimensions % kParts;
		int k2 = kParts - (dimensions % kParts);

		int dimK1 = (int) Math.ceil((double) dimensions / (double) kParts);
		int dimK2 = (int) Math.floor((double) dimensions / (double) kParts);

		int counter = 0;

		for (int i = 0; i < k1; i++) {
			for (int j = 0; j < dimK1; j++) {
				CPSOSK[i].setIndex(j, counter);
				//System.out.print("S " + i + " --> " + CPSOSK[i].getIndex(j) + ", ");
				counter += 1;
			}
			//System.out.println();
		}

		for (int i = k1; i < kParts; i++) {
			for (int j = 0; j < dimK2; j++) {
				CPSOSK[i].setIndex(j, counter);
				//System.out.print("S " + i + " --> " + CPSOSK[i].getIndex(j) + ", ");
				counter += 1;
			}
			//System.out.println();
		}

	}

	public int[] initDimPerSwarm() {

		int[] dimPerSwarm = new int[kParts];

		int k1 = dimensions % kParts;
		int k2 = kParts - (dimensions % kParts);

		int dimK1 = (int) (Math.ceil((double) dimensions / (double) kParts));
		int dimK2 = (int) Math.floor((double) dimensions / (double) kParts);

		//System.out.println(dimK1 + " " + dimK2);

		for (int i = 0; i < k1; i++) {
			dimPerSwarm[i] = dimK1;
			//System.out.print(dimK1 + ", ");
		}

		for (int i = k1; i < kParts; i++) {
			dimPerSwarm[i] = dimK2;
			//System.out.print(dimK2 + ", ");
		}

		//System.out.println();

		return dimPerSwarm;
	}

	public void updateVelocity(Swarm s, int i) {
		Random r = new Random();

		// double inertia = 0.5 + r.nextDouble() / 2;
		double[] particleVelocity = s.getParticle(i).getVelocity();
		double[] particleBestPosition = s.getParticle(i).getBestPosition();
		double[] particlePosition = s.getParticle(i).getCurrentPosition();
		double[] gPosition = s.getBestPosition();

		int dims = 0;

		if (isVanilla) {
			dims = dimensions;
		} else {
			dims = s.getNumDims();
		}

		double[] newVel = new double[dims];
		for (int j = 0; j < dims; j++) {
			double randDouble1 = r.nextDouble();
			double randDouble2 = r.nextDouble();
			newVel[j] = inertia * particleVelocity[j]
					+ c1 * randDouble1 * (particleBestPosition[j] - particlePosition[j])
					+ c2 * randDouble2 * (gPosition[j] - particlePosition[j]);
		}

		s.getParticle(i).setCurrentVelocity(newVel);

	}

	public void updatePosition(Swarm s, int i) {

		double[] particlePosition = s.getParticle(i).getCurrentPosition();
		double[] particleVelocity = s.getParticle(i).getVelocity();

		int dims = 0;

		if (isVanilla) {
			dims = dimensions;
		} else {
			dims = s.getNumDims();
		}

		double[] newPos = new double[dims];

		for (int j = 0; j < dims; j++) {
			newPos[j] = particlePosition[j] + particleVelocity[j];
			double pMax = fn.getMax();
			double pMin = fn.getMin();

			if (newPos[j] > pMax) {
				newPos[j] = pMax;
			} else if (newPos[j] < pMin) {
				newPos[j] = pMin;
			}
		}
		s.getParticle(i).setCurrentPosition(newPos);

	}

	private double fitnessPSO(double[] newPosition) {

		double fnVal = fn.compute(newPosition);
		return fnVal;
	}

}
