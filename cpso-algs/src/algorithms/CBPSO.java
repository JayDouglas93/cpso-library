package algorithms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class CBPSO extends Algorithm {

	private double[] contextVector;

	private Swarm[] swarms;
	private double c1, c2, inertia;
	private int kParts;

	public CBPSO(HashMap<String, String> p) {

		super(p); // Sends HashMap to superclass who then creates global,
					// protected variables

		contextVector = new double[dimensions];

		inertia = Double.parseDouble(p.get("inertia"));
		c1 = Double.parseDouble(p.get("c1"));
		c2 = Double.parseDouble(p.get("c2"));
		kParts = Integer.parseInt(p.get("kParts"));

		fitPerEval = new double[fnEvals];

		initSwarms();
		begin();
	}

	public void begin() {

		int curFnEval = 0;

		while (!endCondition(curFnEval)) {

			if (logging) {
				System.out.println("Iteration " + curFnEval);

				for (int q = 0; q < dimensions; q++) {
					System.out.println("Dimension " + q + " is " + contextVector[q]);
				}
				System.out.println("...");
			}

			double[][] vectorEvals = new double[numParticles][dimensions];

			for (int i = 0; i < kParts; i++) {

				Swarm curSwarm = swarms[i];

				for (int j = 0; j < numParticles; j++) {
					Particle curParticle = curSwarm.getParticle(j);
					double[] curPos = curParticle.getCurrentPosition();
					double[] bestPos = curParticle.getBestPosition();

					if (fitness(curSwarm, contextVector, curPos) < fitness(curSwarm, contextVector, bestPos)) {

						curParticle.setBestPosition(curPos);
					}

					bestPos = curParticle.getBestPosition();

					double[] swarmBestPos = curSwarm.getBestPosition();
					double bestPosFit = fitness(curSwarm, contextVector, bestPos);

					if (bestPosFit < fitness(curSwarm, contextVector, swarmBestPos)) {
						curSwarm.setGlobalBest(bestPos, j);
						// Spot A for contextVector update

						if (bestPosFit < fn.compute(contextVector)) {
							for (int k = 0; k < curSwarm.getNumDims(); k++) {

								contextVector[curSwarm.getIndex(k)] = curSwarm.getBestPosition()[k];
							}
						}
					}

					// OLD FN EVAL UPDATE
					vectorEvals[j] = contextVector.clone();
					/*
					 * for (int k = 0; k < curSwarm.getNumDims(); k++) {
					 * vectorEvals[j][curSwarm.getIndex(k)] =
					 * contextVector[curSwarm.getIndex(k)]; }
					 */
				}
				// Spot B for contextVector update

				// OLD PARTICLE POS, VEL UPDATE
				for (int j = 0; j < numParticles; j++) {
					updatePosition(swarms[i], j);
				}
			}

			for (int j = 0; j < numParticles; j++) {
				if (curFnEval < fnEvals) {
					//System.out.println();
					fitPerEval[curFnEval] = fn.compute(vectorEvals[j]);
					curFnEval += 1;
				}
			}

			// FEB 2018, POS VEL
		}
	}

	public void initSwarms() {

		swarms = new Swarm[kParts];

		int[] dimPerSwarm = initDimPerSwarm();

		for (int i = 0; i < kParts; i += 1) {
			swarms[i] = new Swarm(numParticles, dimPerSwarm[i], fn);
		}

		initDimensions();

		for (int i = 0; i < kParts; i += 1) {
			// Initialize context vector to random particles in each swarm
			Random rand = new Random();
			int index = rand.nextInt(numParticles);
			double[] swarmVal = swarms[i].getParticle(index).getCurrentPosition();

			for (int j = 0; j < dimPerSwarm[i]; j++) {
				contextVector[swarms[i].getIndex(j)] = swarmVal[j];
			}
		}

	}

	public void initDimensions() {

		int k1 = dimensions % kParts;
		int k2 = kParts - (dimensions % kParts);

		int dimK1 = (int) Math.ceil((double) dimensions / (double) kParts);
		int dimK2 = (int) Math.floor((double) dimensions / (double) kParts);

		int counter = 0;

		for (int i = 0; i < k1; i++) {
			for (int j = 0; j < dimK1; j++) {
				swarms[i].setIndex(j, counter);
				//System.out.print("S " + i + " --> " + swarms[i].getIndex(j) + ", ");
				counter += 1;
			}
			//System.out.println();
		}

		for (int i = k1; i < kParts; i++) {
			for (int j = 0; j < dimK2; j++) {
				swarms[i].setIndex(j, counter);
				//System.out.print("S " + i + " --> " + swarms[i].getIndex(j) + ", ");
				counter += 1;
			}
			//System.out.println();
		}

	}

	public int[] initDimPerSwarm() {

		int[] dimPerSwarm = new int[kParts];

		int k1 = dimensions % kParts;
		int k2 = kParts - (dimensions % kParts);

		int dimK1 = (int) (Math.ceil((double) dimensions / (double) kParts));
		int dimK2 = (int) Math.floor((double) dimensions / (double) kParts);

		//System.out.println(dimK1 + " " + dimK2);

		for (int i = 0; i < k1; i++) {
			dimPerSwarm[i] = dimK1;
			//System.out.print(dimK1 + ", ");
		}

		for (int i = k1; i < kParts; i++) {
			dimPerSwarm[i] = dimK2;
			//System.out.print(dimK2 + ", ");
		}

		//System.out.println();

		return dimPerSwarm;
	}

	public void updatePosition(Swarm s, int i) {

		double[] partBestPos = s.getParticle(i).getBestPosition().clone();
		double[] gBest = s.getBestPosition().clone();

		int dims = 0;

		dims = s.getNumDims();

		double[] newPos = new double[dims];

		Random r = new Random();

		r = new Random();
		double gaus = r.nextGaussian();

		double alpha = 0.65;

		for (int j = 0; j < dims; j++) {
			newPos[j] = 0.5 * (partBestPos[j] + gBest[j]) + alpha * gaus * (Math.abs(partBestPos[j] - gBest[j]));
			double pMax = s.getPMax();
			double pMin = s.getPMin();

			if (newPos[j] > pMax) {
				newPos[j] = pMax;
			} else if (newPos[j] < pMin) {
				newPos[j] = pMin;
			}
		}
		s.getParticle(i).setCurrentPosition(newPos);

	}

}
