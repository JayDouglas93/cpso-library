package algorithms;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class DCPSO2 extends Algorithm {

	private double[] contextVector;

	private Swarm[] swarms;
	private SwarmManager swarmManager;

	private double c1, c2, inertia;
	private int kParts;
	private int freqSplit;
	private int numSplit;

	private int regroupMethod;
	private int maxStagnate;
	private int numStagnated;

	public DCPSO2(HashMap<String, String> p) {

		super(p); // Sends HashMap to superclass who then creates global,
					// protected variables

		contextVector = new double[dimensions];

		inertia = Double.parseDouble(p.get("inertia"));
		c1 = Double.parseDouble(p.get("c1"));
		c2 = Double.parseDouble(p.get("c2"));
		// kParts = Integer.parseInt(p.get("kParts"));
		kParts = 1;

		fitPerEval = new double[fnEvals];
		freqSplit = Integer.parseInt(p.get("freqSplit"));
		numSplit = Integer.parseInt(p.get("numSplit"));
		regroupMethod = Integer.parseInt(p.get("regroupMethodD"));
		maxStagnate = Integer.parseInt(p.get("maxStagnateD"));

		int personalGlobalBest = Integer.parseInt(p.get("pgBestParamD"));
		swarmManager = new SwarmManager(personalGlobalBest, numParticles, fn);
		swarmManager.randomlySplit = true;

		initSwarms();
		begin();
	}

	public void begin() {

		int curFnEval = 0;
		int nCounter = 0;
		numStagnated = 0;
		double numParts = (1 + (Math.log(dimensions) / Math.log(numSplit)));

		int n = (int) (1.0 * fnEvals / numParts);
		if (logging) {
			//System.out.println(fnEvals + "/" + (1 + (Math.log(dimensions) / Math.log(numSplit))) + " = " + n);

			//System.out.println("decompose every... " + n);
		}

		while (!endCondition(curFnEval)) {

			if (logging) {
				System.out.println("Iteration " + curFnEval);

				// for (int q = 0; q < dimensions; q++) {
				// System.out.println("Dimension " + q + " is " +
				// contextVector[q]);
				// }
				System.out.println("...");
			}

			switch (regroupMethod) {
			case 0:
				nCounter = checkStaticRegroup(nCounter, n);
				break;
			case 1:
				checkStagnated();
			}

			double[][] vectorEvals = new double[numParticles][dimensions];

			for (int d = 0; d < kParts; d++) {

				Swarm curSwarm = swarms[d];
				boolean isStagnated = true;

				for (int p = 0; p < curSwarm.getSize(); p++) {
					Particle curParticle = curSwarm.getParticle(p);

					double[] curPos = curParticle.getCurrentPosition();
					double[] bestPos = curParticle.getBestPosition();

					if (fitness(curSwarm, contextVector, curPos) < fitness(curSwarm, contextVector, bestPos)) {
						curParticle.setBestPosition(curPos);
					}

					bestPos = curParticle.getBestPosition();

					double[] swarmBestPos = curSwarm.getBestPosition();

					if (fitness(curSwarm, contextVector, bestPos) < fitness(curSwarm, contextVector, swarmBestPos)) {

						isStagnated = false;
						curSwarm.setGlobalBest(bestPos, p);

						// Spot A for context vector update

						if (fitness(curSwarm, contextVector, swarmBestPos) < fn.compute(contextVector)) {
							for (int i = 0; i < curSwarm.getNumDims(); i++) {
								contextVector[curSwarm.getIndex(i)] = swarmBestPos[i];
							}
						}
					}

					// OLD SPOT FOR FN EVAL UPDATE
					vectorEvals[p] = contextVector.clone();
					/*
					 * for (int k = 0; k < curSwarm.getNumDims(); k++) {
					 * vectorEvals[p][curSwarm.getIndex(k)] =
					 * contextVector[curSwarm.getIndex(k)]; }
					 */

				}
				// Spot B for context vector update

				// OLD POS VEL UPDATE

				for (int p = 0; p < curSwarm.getSize(); p++) {
					updateVelocity(swarms[d], p);
					updatePosition(swarms[d], p);
				}

				if (isStagnated && curSwarm.getNumDims() != 1) {
					// System.out.println("Swarm "+d+" stangated for
					// "+curSwarm.numStagnate);
					curSwarm.numStagnate += 1;
					if (curSwarm.numStagnate == maxStagnate) {
						numStagnated += 1;
					}
				}else{
					curSwarm.numStagnate = 0;
				}
			}

			for (int j = 0; j < numParticles; j++) {
				if (curFnEval < fnEvals) {
					fitPerEval[curFnEval] = fn.compute(vectorEvals[j]);
					curFnEval += 1;
				}
				nCounter += 1;
			}

			// FEB 2018, POS VEL

		}

		// TESTING
		/*
		 * for (int i = 0; i < dimensions; i++) { //
		 * System.out.println("Dimension "+i+ " is "+contextVector[i]); //
		 * ystem.out.println(fitness(i, contextVector[i])); }
		 */
	}

	public void initSwarms() {

		swarms = new Swarm[kParts];
		int[] dimPerSwarm = initDimPerSwarm();

		for (int i = 0; i < kParts; i += 1) {
			swarms[i] = new Swarm(numParticles, dimPerSwarm[i], fn);
		}

		initDimensions();

		for (int i = 0; i < kParts; i += 1) {
			// Initialize context vector to random particles in each swarm
			Random rand = new Random();
			int index = rand.nextInt(numParticles);
			double[] swarmVal = swarms[i].getParticle(index).getCurrentPosition();

			for (int j = 0; j < dimPerSwarm[i]; j++) {
				contextVector[swarms[i].getIndex(j)] = swarmVal[j];
			}
		}

	}

	public void initDimensions() {

		int k1 = dimensions % kParts;
		int k2 = kParts - (dimensions % kParts);

		int dimK1 = (int) Math.ceil((double) dimensions / (double) kParts);
		int dimK2 = (int) Math.floor((double) dimensions / (double) kParts);

		int counter = 0;

		for (int i = 0; i < k1; i++) {
			for (int j = 0; j < dimK1; j++) {
				swarms[i].setIndex(j, counter);
				// System.out.print("S " + i + " --> " + swarms[i].getIndex(j) +
				// ", ");
				counter += 1;
			}
			// System.out.println();
		}

		for (int i = k1; i < kParts; i++) {
			for (int j = 0; j < dimK2; j++) {
				swarms[i].setIndex(j, counter);
				// System.out.print("S " + i + " --> " + swarms[i].getIndex(j) +
				// ", ");
				counter += 1;
			}
			// System.out.println();
		}

	}

	public int[] initDimPerSwarm() {

		int[] dimPerSwarm = new int[kParts];

		int k1 = dimensions % kParts;
		int k2 = kParts - (dimensions % kParts);

		int dimK1 = (int) (Math.ceil((double) dimensions / (double) kParts));
		int dimK2 = (int) Math.floor((double) dimensions / (double) kParts);

		// System.out.println(dimK1 + " " + dimK2);

		for (int i = 0; i < k1; i++) {
			dimPerSwarm[i] = dimK1;
			// System.out.print(dimK1 + ", ");
		}

		for (int i = k1; i < kParts; i++) {
			dimPerSwarm[i] = dimK2;
			// System.out.print(dimK2 + ", ");
		}

		// System.out.println();

		return dimPerSwarm;
	}

	public void updateVelocity(Swarm s, int i) {
		Random r = new Random();

		// double inertia = 0.5 + r.nextDouble() / 2;
		double[] particleVelocity = s.getParticle(i).getVelocity();
		double[] particleBestPosition = s.getParticle(i).getBestPosition();
		double[] particlePosition = s.getParticle(i).getCurrentPosition();
		double[] gPosition = s.getBestPosition();

		double[] newVel = new double[s.getNumDims()];
		// System.out.println("Swarm "+si+", updating vel...");
		for (int j = 0; j < s.getNumDims(); j++) {
			// System.out.println("Dimension "+j+": pbest is
			// "+particleBestPosition[j]+", gbest is "+gPosition[j]);
			double randDouble1 = r.nextDouble();
			double randDouble2 = r.nextDouble();
			newVel[j] = inertia * particleVelocity[j]
					+ c1 * randDouble1 * (particleBestPosition[j] - particlePosition[j])
					+ c2 * randDouble2 * (gPosition[j] - particlePosition[j]);
		}

		s.getParticle(i).setCurrentVelocity(newVel);

	}

	public void updatePosition(Swarm s, int i) {

		// System.out.println("Swarm "+si+", updating positions...");
		double[] particlePosition = s.getParticle(i).getCurrentPosition();
		double[] particleVelocity = s.getParticle(i).getVelocity();
		double[] newPos = new double[s.getNumDims()];

		for (int j = 0; j < s.getNumDims(); j++) {
			// System.out.println("Index "+j+": vel is "+particleVelocity[j]);
			newPos[j] = particlePosition[j] + particleVelocity[j];
			double pMax = fn.getMax();
			double pMin = fn.getMin();

			if (newPos[j] > pMax) {
				newPos[j] = pMax;
			} else if (newPos[j] < pMin) {
				newPos[j] = pMin;
			}
		}
		s.getParticle(i).setCurrentPosition(newPos);

	}

	private double summation(int n) {

		double counter = 0;

		for (int i = 1; i <= n; i++) {
			counter += 1.0 / i;
		}

		double result = fnEvals / counter;// + ((maxIterations%counter == 0)
											// ? 0 : 1);
		return result;

	}

	private int checkStaticRegroup(int count, int max) {

		if ((count >= max) && (swarms.length != dimensions)) {

			if (logging) {
				System.out.println("Splitting...");
			}
			int numNewSwarms = swarms.length * numSplit;
			if (numNewSwarms > dimensions) {
				numNewSwarms = dimensions;
			}

			// Used to determine new structure
			int k1 = dimensions % numNewSwarms;
			int s1 = (int) Math.ceil((double) dimensions / (double) numNewSwarms);
			int s2 = (int) Math.floor((double) dimensions / (double) numNewSwarms);

			int[] dimPerSwarm = new int[numNewSwarms];

			for (int s = 0; s < numNewSwarms; s++) {
				if (s < k1) {
					dimPerSwarm[s] = s1;
				} else {
					dimPerSwarm[s] = s2;
				}
			}

			swarms = swarmManager.changeK(swarms, dimensions, dimPerSwarm);

			count = 0;
			kParts = swarms.length; // kParts *= parts;

			// System.out.println("kParts: "+kParts);

		} // end splitting

		return count;

	}

	private void checkStagnated() {

		if (numStagnated != 0) {

			int[] dimPerSwarm = new int[swarms.length + numStagnated];
			int dpsIndex = 0;

			for (int s = 0; s < swarms.length; s++) {

				Swarm curSwarm = swarms[s];

				// System.out.println("Check: swarm "+s+" stangated for
				// "+curSwarm.numStagnate);

				if (curSwarm.numStagnate == maxStagnate && curSwarm.getNumDims() != 1) {

					dimPerSwarm[dpsIndex] = (int) Math.floor(curSwarm.getNumDims() / 2.0);
					dimPerSwarm[dpsIndex + 1] = (int) Math.ceil(curSwarm.getNumDims() / 2.0);
					dpsIndex += 2;

				} else {
					dimPerSwarm[dpsIndex] = curSwarm.getNumDims();
					dpsIndex += 1;
				}
			}

			if (logging) {
				System.out.println("Swarms stagnated, new structure:");
				for (int i = 0; i < dimPerSwarm.length; i++) {
					System.out.print(dimPerSwarm[i] + " ");
				}
				System.out.println();
			}

			swarms = swarmManager.changeK(swarms, dimensions, dimPerSwarm);

			kParts = dimPerSwarm.length;
			// Stagnated swarms have now be split/merged, so reset counter
			numStagnated = 0;
		}
	}

}
