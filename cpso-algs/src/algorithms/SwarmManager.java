package algorithms;

import java.util.ArrayList;
import java.util.Collections;

import cecfunctions2010.Function;

//This class will be used to merge and split swarms
//Not necessary until dCPSO and mCPSO

public class SwarmManager {

	private int pgBestParam;
	public boolean randomlySplit;
	private int swarmSize;
	private Function testFn;

	public SwarmManager(int pgbp, int ss, Function tfn) {
		pgBestParam = pgbp;
		swarmSize = ss;
		testFn = tfn;
		randomlySplit = false;
	}

	// Intially will be used by CCPSO2 to dynamically merge or
	// split swarms, depending on current size
	// Note: Will only work with even splits/merges
	public void changeS(Swarm[] swarms, int n, int s) {
		int k = n / s;

	}

	public Swarm[] changeK(Swarm[] swarms, int n, int[] dimPerSwarm) {

		int k = dimPerSwarm.length;

		Swarm[] newSwarms = new Swarm[k];

		// Used to keep track for merging dim indices
		int curS = 0;
		int curSDim = 0;

		// Since dimindices can move to next swarm before particles have been
		// copied
		// need separate counter
		int startSwarm = 0;
		int startDim = 0;

		// To incorporate random splitting, create a random mask in each swarm
		// for dimensions to randomly be selected

		if (randomlySplit) {
			for (Swarm s : swarms) {
				s.getRandMask();
			}
		}

		System.out.println("Changing " + swarms.length + " into " + dimPerSwarm.length);

		// FFirst and second half in one step
		for (int i = 0; i < k; i++) {
			// System.out.println("Swarm "+i);

			// How many dims does current swarm have?
			int curDims = dimPerSwarm[i];

			// Test this: might not work in some cases

			Swarm s = null;

			if (curDims == swarms[curS].getNumDims() && curSDim == 0) {

				// If the swarm isn't being split or merged... just use the last
				// swarm

				s = swarms[curS];

				curS += 1;
				curSDim = 0;

				startSwarm += 1;
				startDim = 0;

			} else {

				// Create mask to select dimensions from

				/*
				 * int maskCount = 0; ArrayList<Integer> mask = new
				 * ArrayList<Integer>();
				 * 
				 * for (int r = curSDim; r < (curSDim+curDims); r++) {
				 * mask.add(r); } //if (mask.get(mask.size()-1) >=
				 * swarms[curS].getNumDims() ){ // mask.set((mask.size()-1), 0);
				 * //}
				 * 
				 * if (randomlySplit){ Collections.shuffle(mask); } for (Integer
				 * r : mask){ System.out.print(r+", "); } System.out.println();
				 */

				// System.out.println("has: "+curDims+" dimensions");
				// For swarm i, get ready to merge dim indices
				int[] newDimIndices = new int[curDims];

				for (int d = 0; d < curDims; d++) {
					// System.out.println("s: "+curSDim);

					if (randomlySplit) {
						newDimIndices[d] = swarms[curS].getIndex(swarms[curS].mask.get(curSDim));
					} else {
						newDimIndices[d] = swarms[curS].getIndex(curSDim);
					}
					// newDimIndices[d] =
					// swarms[curS].getIndex(mask.get(maskCount));
					// System.out.println("curSDim: "+curSDim+" vs mask:
					// "+index);

					//maskCount += 1;
					curSDim += 1;
					// If you've reached the end of the current swarm
					// increment swarm counter to look at next one
					// reset dimension counter for next swarm
					if (curSDim == swarms[curS].getDimIndices().length) {
						curS += 1;
						curSDim = 0;
					}
				}

				//maskCount = 0;

				Particle[] newParticles = new Particle[swarmSize];
				int curSP = 0;
				int curSDimP = 0;
				for (int p = 0; p < swarmSize; p++) {

					// System.out.println("\np: "+p+", swarmsize: "+swarmSize);

					double[] newPartPos = new double[curDims];
					double[] newPartVel = new double[curDims];

					curSP = startSwarm;
					curSDimP = startDim;

					for (int d = 0; d < curDims; d++) {
						// System.out.println("s: "+curSP+", dim: "+curSDimP);

						if (pgBestParam == 0) {
							if (randomlySplit){
								newPartPos[d] = swarms[curSP].getParticle(p).getCurrentPosition()[swarms[curSP].mask.get(curSDimP)];
								newPartVel[d] = swarms[curSP].getParticle(p).getVelocity()[swarms[curSP].mask.get(curSDimP)];
							}
							else{
								newPartPos[d] = swarms[curSP].getParticle(p).getCurrentPosition()[curSDimP];
								newPartVel[d] = swarms[curSP].getParticle(p).getVelocity()[curSDimP];
							}
						} else if (pgBestParam == 1) {
							newPartPos[d] = swarms[curSP].getParticle(p).getRandom();
							newPartVel[d] = swarms[curSP].getParticle(p).getRandom();
						}

						curSDimP += 1;
						//maskCount += 1;
						// If you've reached the end of the current swarm
						// increment swarm counter to look at next one
						// reset dimension counter for next swarm
						if (curSDimP == swarms[curSP].getDimIndices().length) {
							curSP += 1;
							curSDimP = 0;
							//maskCount = 0;
						}
					}
					Particle newP = new Particle(newPartPos, newPartVel, testFn.getMin(), testFn.getMax());
					newParticles[p] = newP;
				}
				startSwarm = curSP;
				startDim = curSDimP;

				s = new Swarm(swarmSize, newDimIndices, newParticles, testFn);

			}
			newSwarms[i] = s;
		}

		return newSwarms;

	}

	public void moveOutsideRadius(Swarm[] swarms, double radius) {

		for (int s = 0; s < swarms.length; s++) {
			swarms[s].moveOutsideRadius(radius);
		}
	}

}
