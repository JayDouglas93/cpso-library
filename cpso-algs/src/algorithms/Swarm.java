package algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

//import functions.Function;
import cecfunctions2010.Function;

public class Swarm {

	//private Function testFn;
	private double fnMin;
	private double fnMax;
	
	private double[] bestPosition;
	private double[] worstPosition;
	private double[][] lBestPosition;
	private Particle[] particles;
	private int size;
	private int[] dimIndices;
	
	public int numStagnate;

	private int bpIndex; //index of best particle
	private int wpIndex; //index of worst particle
	
	private double[] lastPosition; //Used to tell just how much a swarm has improved
	private double distFromLastPos;
	private boolean justRegrouped;
	
	public ArrayList<Integer> mask;

	//Regular swarm creation
	public Swarm(int s, int dimPerSwarm, Function tf) {
		particles = null;
		size = s;
		dimIndices = new int[dimPerSwarm];
		
		//testFn = tf;
		fnMin = tf.getMin();
		fnMax = tf.getMax();
		
		// bestPosition = new double[dimPerSwarm];
		initSwarm(dimPerSwarm);
		
		numStagnate = 0;
		
		justRegrouped = true;
	}

	// Constructor for splitting / merging swarms
	public Swarm(int s, int[] di, Particle[] ps, Function tf) {
		// size = p.length;
		size = s;
		dimIndices = di.clone();
		particles = ps;
		
		//testFn = tf;
		fnMin = tf.getMin();
		fnMax = tf.getMax();

		// initSwarm(di.length);
		
		//If new swarm doesn't have a best, it breaks, so choose random particle
		Random rand = new Random();
		int x1 = rand.nextInt((size - 1 - 0) + 1) + 0;
		bestPosition = particles[x1].getCurrentPosition();
		int x2 = rand.nextInt((size - 1 - 0) + 1) + 0;
		worstPosition = particles[x2].getCurrentPosition();
		bpIndex = x1;
		wpIndex = x2;
		
		numStagnate = 0;
		
		justRegrouped = true;
	}

	//Cloning
	public Swarm( Swarm other ) {
		
		this.fnMin = other.fnMin;
		this.fnMax = other.fnMax;

		// bestParticle = bp;
		this.bestPosition = other.bestPosition.clone();
		this.worstPosition = other.bestPosition.clone();
		this.dimIndices = other.dimIndices.clone();
		this.bpIndex = other.bpIndex;
		this.wpIndex = other.wpIndex;
		this.size = other.size;
		
		this.particles = new Particle[this.size];
		
		for (int i=0 ; i<this.size ; i++){
			this.particles[i] = new Particle(other.particles[i]);
		}
		
		this.numStagnate = other.numStagnate;
		this.justRegrouped = other.justRegrouped;
		
	}
	
	//Used when you need to manually specify min and max of particles
	public Swarm(int s, int dimPerSwarm, double pmn, double pmx) {
		particles = null;
		size = s;
		dimIndices = new int[dimPerSwarm];
		
		//testFn = tf;
		fnMin = pmn;
		fnMax = pmx;
		
		initSwarm(dimPerSwarm);
		
		numStagnate = 0;
	}

	
	public double[] getLastPosition(){
		return lastPosition.clone();
	}
	
	public double[] getBestPosition() {
		return bestPosition.clone();
	}
	public double[] getWorstPosition() {
		return worstPosition.clone();
	}
	
	public Particle[] getParticles(){
		return particles;
	}

	public void setIndex(int i, int j) {
		dimIndices[i] = j;
	}
	public double getPMin(){
		return fnMin;
	}
	public double getPMax(){
		return fnMax;
	}

	public int[] getDimIndices() {
		return dimIndices.clone();
	}
	
	public int getNumDims(){
		return dimIndices.length;
	}

	public int getIndex(int i) {
		return dimIndices[i];
	}
	
	public double getDistFromLast(){
		return distFromLastPos;
	}

	public void setGlobalBest(double[] newBest, int bpi) {
		bestPosition = newBest.clone();
		bpIndex = bpi;
		// bestParticle = newBest;
		// bpIndex = index;
	}
	
	public void updateGlobalBest(int index, double val){
		bestPosition[index] = val;
	}
	
	public void setGlobalWorst(double[] newWorst, int wpi) {
		worstPosition = newWorst.clone();
		wpIndex = wpi;
		// bestParticle = newBest;
		// bpIndex = index;
	}
	
	public void updateGlobalWorst(int index, double val){
		worstPosition[index] = val;
	}

	public int getSize() {
		return particles.length;
	}

	public int getBpIndex() {
		return bpIndex;
	}
	
	public int getWpIndex() {
		return wpIndex;
	}

	public void setSize(int s) {
		size = s;
	}

	public Particle getParticle(int index) {
		return particles[index];
	}

	public void setParticle(int index, Particle p) {
		particles[index] = p;
	}
	
	public void setParticlePos(int index, double[] pos) {
		particles[index].setCurrentPosition(pos) ;
	}
	
	public void setLBest(int pIndex, double[] pos){
		lBestPosition[pIndex] = pos.clone();
	}
	
	public double[] getLBest(int pIndex){
		return lBestPosition[pIndex].clone();
	}
	
	public void setLastPosition( double[] lastPos ){
		lastPosition = lastPos.clone();
	}
	
	public void setDistFromLast( double dist ){
		
		distFromLastPos = dist;
		
	}
	
	public boolean isNew(){
		return justRegrouped;
	}
	
	public void setNew( boolean bool ){
		justRegrouped = bool;
	}

	private void initSwarm(int dimPerSwarm) {

		lBestPosition = new double[size][dimPerSwarm];
		
		if (particles == null) {
			particles = new Particle[size];
			for (int i = 0; i < size; i++) {
				particles[i] = new Particle(dimPerSwarm, fnMin, fnMax);
			}
		}
		Random rand = new Random();
		int x1 = rand.nextInt((size - 1 - 0) + 1) + 0;

		/*
		 * for (int i = 0; i < dimPerSwarm; i++) { bestPosition[i] =
		 * Integer.MAX_VALUE; }
		 */
		// bestParticle = particles[x];
		bestPosition = particles[x1].getCurrentPosition();
		bpIndex = x1;
		
		int x2 = rand.nextInt((size - 1 - 0) + 1) + 0;
		worstPosition = particles[x2].getCurrentPosition();
		wpIndex = x2;
		
		lastPosition = bestPosition.clone();
	}
	
	public void moveOutsideRadius( double radius ){
		
		for (int p = 0 ; p < size ; p++){
			
			Particle part = particles[p];
			part.moveOutsideRadius(radius);
			
			//So far... leave updating of best particle, etc, to fitness loop
		}
		
	}
	
	public void getRandMask(){
		mask = new ArrayList<Integer>();
		
		for ( int i = 0 ; i < dimIndices.length ; i++ ){
			mask.add(i);
		}
		Collections.shuffle(mask);
	}

	
}
