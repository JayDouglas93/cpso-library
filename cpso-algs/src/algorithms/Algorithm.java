package algorithms;

import java.io.File;
import java.util.HashMap;
import java.util.Random;

//import functions.*;
import cecfunctions2010.*;

public abstract class Algorithm {
	
	private HashMap<String, String> params;
	
	protected int dimensions;
	
	protected int fnEvals;
	
	protected int runs;
	protected int numParticles;
	
	protected double inertia;
	
	protected double[] fitPerEval;
	
	protected Function fn;
	protected String fnName;
	
	protected boolean isRotated;
	protected boolean logging;
	
	/**
	 * Constructor will be called to initialize algorithm
	 * parameters when super() is called from subclass
	 * @param p - Hashmap of the parameter set
	 */
	protected Algorithm(HashMap<String, String> p){
		
		params = p;
		dimensions = Integer.parseInt(p.get("dimensions"));
		fnEvals = Integer.parseInt(params.get("functionEvals"));
		runs = Integer.parseInt(p.get("runs"));
		numParticles = Integer.parseInt(p.get("numParticles"));
		isRotated = Boolean.parseBoolean(p.get("rotated"));
		logging = Boolean.parseBoolean(p.get("logging"));
		
		String fnDir = p.get("functionDir");
		
		fnName = p.get("functions");
		
		Defaults.DEFAULT_DIM = dimensions;
		Defaults.DEFAULT_M = (int) Math.ceil((1.0*dimensions)*0.05);
		
		try { //Converts function string name to class object
			Class<?> cls = Class.forName(fnDir+"."+fnName);
			Object obj = cls.newInstance();
			fn = Function.class.cast(obj);
			  
		  	System.out.println("DIMENSIONS: "+fn.getDimension()+", "+dimensions+", "+Defaults.DEFAULT_DIM);
		  	System.out.println("# of related vars: "+Defaults.DEFAULT_M);
			
			/*if (isRotated){
				double[][] rotMatrix = fn.getRandomRotation(dimensions);
				fn.setRotated(true);
				fn.setRandomRotation(rotMatrix);
			}
			else{
				fn.setRotated(false);
			}*/
			
			
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		
		
	}
	
	
	public double getEndFitness(){
		
		return fitPerEval[fnEvals-1];
	}
	
	public double[] getFitPerEval(){
		return fitPerEval;
	}
	
	protected boolean endCondition(int cur){
		
		int max = fnEvals;
		
		return cur >= max;
		
	}
	
	protected double fitness(Swarm swarm, double[] contextVector, double[] newPosition){
		
		double[] cloneCVector = contextVector.clone();
		
		for (int j = 0; j < swarm.getNumDims(); j++) {
			cloneCVector[swarm.getIndex(j)] = newPosition[j];
		}

		double fnVal = 0;

		fnVal = fn.compute(cloneCVector);

		return fnVal;
	}
	
	public abstract void begin();
	
	public abstract void initSwarms();
	
	public abstract void initDimensions();
	
	public abstract int[] initDimPerSwarm();
	
	public abstract void updatePosition(Swarm s, int i);
	
}
