package algorithms;

import java.util.Random;

public class Particle {

	private double[] position;
	private double[] velocity;
	private double[] bestPosition;
	
	private double pMin;
	private double pMax;
	
	private boolean best;
	
	private double fitness;

	//Used to create new particle
	public Particle(int dimPerSwarm, double pMin, double pMax) {
		position = new double[dimPerSwarm];
		velocity = new double[dimPerSwarm];

		this.pMin = pMin;
		this.pMax = pMax;
		//setMinMax();

		initParticle(dimPerSwarm);
	}

	//Used by SwarmManager to state to explicitly set the position and velocity
	public Particle(double[] p, double[] v, double pMin, double pMax) {
		position = p.clone();
		velocity = v.clone();
		bestPosition = position.clone();
		this.pMin = pMin;
		this.pMax = pMax;
	}

	//Used for copying
	public Particle(Particle other) {
		this.position = other.position.clone();
		this.velocity = other.velocity.clone();
		this.bestPosition = other.bestPosition.clone();
		this.pMin = other.pMin;
		this.pMax = other.pMax;
		//setMinMax();
	}

	public double[] getCurrentPosition() {
		return position.clone();
	}

	public double[] getBestPosition() {
		return bestPosition.clone();
	}

	public double[] getVelocity() {
		return velocity.clone();
	}

	public double getPMin() {
		return pMin;
	}

	public double getPMax() {
		return pMax;
	}

	public double getRandom() {
		Random rand = new Random();
		double val = pMin + (pMax - pMin) * rand.nextDouble();

		return val;

	}

	public void setBestPosition(double[] newPosition) {
		bestPosition = newPosition.clone();
	}

	public void setCurrentPosition(double[] newPosition) {
		position = newPosition.clone();
	}

	public void setCurrentVelocity(double[] newVelocity) {
		velocity = newVelocity.clone();
	}

	private void initParticle(int dimPerSwarm) {
		// System.out.println("Initial position & velocity: ");
		for (int i = 0; i < dimPerSwarm; i++) {
			Random rand = new Random();
			double x = pMin + (pMax - pMin) * rand.nextDouble();
			position[i] = x;
			// System.out.print(position[i]+", ");
			// System.out.print(velocity[i]);

			velocity[i] = 0;

		}

		bestPosition = position.clone();
		/*
		 * System.out.print("\nInitial best position: "); for (int
		 * i=0;i<dimPerSwarm;i++){ System.out.print( bestPosition[i]+", "); }
		 * System.out.println("\n");
		 */
	}
	
	public boolean isBest(){
		return best;
	}
	
	public void setBest(boolean b){
		best = b;
	}
	
	public void setFitness( double f ){
		fitness = f;
	}
	
	public double getFitness (){
		
		return fitness;
	}
	
	public void moveOutsideRadius( double radius ){
		
		Random rand = new Random();
		
		for (int d = 0 ; d < position.length ; d++){
			double avoidMin = position[d] - radius;
			double avoidMax = position[d] + radius;
			
			if ( avoidMin < pMin ){
				avoidMin = pMin;
			}
			if ( avoidMax > pMax ){
				avoidMax = pMax;
			}
			
			//Instead of rolling random and checking every time, give both sides of radius equal prob
			//and then randomly roll again in either before or after radius
			
			//Possible radius goes past bounds, so if it does, don't push particle to this side (avoidMin != pMin)
			//This is fine since we'll assume radius doesn't go past both min and max
			
			double newPos = Double.MAX_VALUE;
			
			if (rand.nextDouble() < 0.5 && avoidMin != pMin){ //before
				newPos = pMin + (avoidMin - pMin) * rand.nextDouble();
			}
			else{ //after
				newPos = avoidMax + (pMax - avoidMax) * rand.nextDouble();
			}
			
			//System.out.println("d: "+d+", radius: "+radius+", old: "+position[d]+", new: "+newPos);
			
			position[d] = newPos;
			
			//Don't change velocity or best position
			//best position may or may not be this new position, but the fitness loop will check that for us
		}
		
	}
}
