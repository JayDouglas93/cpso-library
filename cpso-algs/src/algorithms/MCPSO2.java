package algorithms;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

public class MCPSO2 extends Algorithm {

	private double[] contextVector;

	private Swarm[] swarms;
	private SwarmManager swarmManager;

	private double c1, c2, inertia;
	private int kParts;
	private int freqMerge;
	private int numMerge;

	private int regroupMethod;
	private int maxStagnate;

	private ArrayList<Integer> stagnatedSwarms;

	public MCPSO2(HashMap<String, String> p) {

		super(p); // Sends HashMap to superclass who then creates global,
					// protected variables

		contextVector = new double[dimensions];

		inertia = Double.parseDouble(p.get("inertia"));
		c1 = Double.parseDouble(p.get("c1"));
		c2 = Double.parseDouble(p.get("c2"));
		// kParts = Integer.parseInt(p.get("kParts"));
		kParts = dimensions;

		fitPerEval = new double[fnEvals];
		freqMerge = Integer.parseInt(p.get("freqMerge"));
		numMerge = Integer.parseInt(p.get("numMerge"));
		regroupMethod = Integer.parseInt(p.get("regroupMethodM"));
		maxStagnate = Integer.parseInt(p.get("maxStagnateM"));

		int personalGlobalBest = Integer.parseInt(p.get("pgBestParamM"));
		swarmManager = new SwarmManager(personalGlobalBest, numParticles, fn);

		stagnatedSwarms = new ArrayList<Integer>();

		initSwarms();
		begin();
	}

	public void begin() {

		int curFnEval = 0;
		int nCounter = 0;

		double numParts = (1 + (Math.log(dimensions) / Math.log(numMerge)));

		int n = (int) (1.0 * fnEvals / numParts);

		if (logging) {
			//System.out.println(fnEvals + "/" + (1 + (Math.log(dimensions) / Math.log(numMerge))) + " = " + n);

			//System.out.println("Merge after... " + n);
		}
		while (!endCondition(curFnEval)) {

			if (logging) {
				System.out.println("Iteration " + curFnEval);

				// for (int i = 0; i < dimensions; i++) {
				// System.out.println("Dimension " + i + " is " +
				// contextVector[i]);
				// }
			}
			// NEW MERGE SPOT, BEFORE FITNESS EVAL
			// Merging every n iterations

			switch (regroupMethod) {
			case 0:
				nCounter = checkStaticRegroup(nCounter, n);
				break;
			case 1:
				checkStagnated();
			}

			double[][] vectorEvals = new double[numParticles][dimensions];

			for (int d = 0; d < kParts; d++) {

				Swarm curSwarm = swarms[d];
				boolean isStagnated = true;

				for (int p = 0; p < curSwarm.getSize(); p++) {
					Particle curParticle = curSwarm.getParticle(p);

					double[] curPos = curParticle.getCurrentPosition();
					double[] bestPos = curParticle.getBestPosition();

					if (fitness(curSwarm, contextVector, curPos) < fitness(curSwarm, contextVector, bestPos)) {
						curParticle.setBestPosition(curPos);
					}

					bestPos = curParticle.getBestPosition();

					double[] swarmBestPos = curSwarm.getBestPosition();

					if (fitness(curSwarm, contextVector, bestPos) < fitness(curSwarm, contextVector, swarmBestPos)) {

						isStagnated = false;
						curSwarm.setGlobalBest(bestPos, p);

						// Stuff for keeping track of distance improvement: NOT
						// NECESSARY
						/*
						 * if (curSwarm.isNew()) { System.out.println("Swarm "
						 * +d+" is new, noting new best, which is "+);
						 * curSwarm.setLastPosition(swarmBestPos);
						 * curSwarm.setDistFromLast(0); } else {
						 * 
						 * double dist =
						 * calcDistFromLast(curSwarm.getLastPosition(),
						 * swarmBestPos); curSwarm.setDistFromLast(dist);
						 * System.out.println("Swarm "
						 * +d+" has moved a total of "+dist); }
						 */

						// Spot A for context vector update

						if (fitness(curSwarm, contextVector, swarmBestPos) < fn.compute(contextVector)) {
							for (int k = 0; k < curSwarm.getNumDims(); k++) {
								contextVector[curSwarm.getIndex(k)] = swarmBestPos[k];
							}
						}
					}

					// OLD SPOT FOR FN EVAL UPDATE
					vectorEvals[p] = contextVector.clone();
					/*
					 * for (int k = 0; k < curSwarm.getNumDims(); k++) {
					 * vectorEvals[p][curSwarm.getIndex(k)] =
					 * contextVector[curSwarm.getIndex(k)]; }
					 */
				}
				// Spot B for context vector update
				// OLD POS VEL UPDATE

				// Part 2 of new swarm: We've gone through all particles to find
				// the true best pos
				// so make sure next time, we're able to calc our dist from this
				// new last pos

				for (int p = 0; p < curSwarm.getSize(); p++) {
					updateVelocity(swarms[d], p);
					updatePosition(swarms[d], p);
				}

				if (isStagnated && curSwarm.getNumDims() != dimensions) {
					curSwarm.numStagnate += 1;
					// System.out.println("Swarm "+d+" stangated for
					// "+curSwarm.numStagnate);
					if (curSwarm.numStagnate == maxStagnate) {
						stagnatedSwarms.add(d);
					}
				}
				else{
					curSwarm.numStagnate = 0;
				}
			}

			for (int j = 0; j < numParticles; j++) {
				if (curFnEval < fnEvals) {
					fitPerEval[curFnEval] = fn.compute(vectorEvals[j]);
					curFnEval += 1;
				}
				nCounter += 1;
			}

			// FEB 2018, POS VEL

			// OLD MERGING SPOT
			// GLOBAL BEST AND PERSONAL BESTS WERE RESET, NEED TO DO BEFORE
			// FITNESS EVAL

		}
	}

	public void initSwarms() {

		swarms = new Swarm[kParts];
		int[] dimPerSwarm = initDimPerSwarm();

		for (int i = 0; i < kParts; i += 1) {
			swarms[i] = new Swarm(numParticles, dimPerSwarm[i], fn);
		}

		initDimensions();

		for (int i = 0; i < kParts; i += 1) {
			// Initialize context vector to random particles in each swarm
			Random rand = new Random();
			int index = rand.nextInt(numParticles);
			double[] swarmVal = swarms[i].getParticle(index).getCurrentPosition();

			for (int j = 0; j < dimPerSwarm[i]; j++) {
				contextVector[swarms[i].getIndex(j)] = swarmVal[j];
			}
		}

	}

	public void initDimensions() {

		int k1 = dimensions % kParts;
		int k2 = kParts - (dimensions % kParts);

		int dimK1 = (int) Math.ceil((double) dimensions / (double) kParts);
		int dimK2 = (int) Math.floor((double) dimensions / (double) kParts);

		int counter = 0;

		for (int i = 0; i < k1; i++) {
			for (int j = 0; j < dimK1; j++) {
				swarms[i].setIndex(j, counter);
				// System.out.print("S " + i + " --> " + swarms[i].getIndex(j) +
				// ", ");
				counter += 1;
			}
			// System.out.println();
		}

		for (int i = k1; i < kParts; i++) {
			for (int j = 0; j < dimK2; j++) {
				swarms[i].setIndex(j, counter);
				// System.out.print("S " + i + " --> " + swarms[i].getIndex(j) +
				// ", ");
				counter += 1;
			}
			// System.out.println();
		}
	}

	public int[] initDimPerSwarm() {

		int[] dimPerSwarm = new int[kParts];

		int k1 = dimensions % kParts;
		int k2 = kParts - (dimensions % kParts);

		int dimK1 = (int) (Math.ceil((double) dimensions / (double) kParts));
		int dimK2 = (int) Math.floor((double) dimensions / (double) kParts);

		// System.out.println(dimK1 + " " + dimK2);

		for (int i = 0; i < k1; i++) {
			dimPerSwarm[i] = dimK1;
			// System.out.print(dimK1 + ", ");
		}

		for (int i = k1; i < kParts; i++) {
			dimPerSwarm[i] = dimK2;
			// System.out.print(dimK2 + ", ");
		}

		// System.out.println();

		return dimPerSwarm;
	}

	public void updateVelocity(Swarm s, int i) {
		Random r = new Random();

		// double inertia = 0.5 + r.nextDouble() / 2;
		double[] particleVelocity = s.getParticle(i).getVelocity();
		double[] particleBestPosition = s.getParticle(i).getBestPosition();
		double[] particlePosition = s.getParticle(i).getCurrentPosition();
		double[] gPosition = s.getBestPosition();

		double[] newVel = new double[s.getNumDims()];
		for (int j = 0; j < s.getNumDims(); j++) {
			double randDouble1 = r.nextDouble();
			double randDouble2 = r.nextDouble();
			newVel[j] = inertia * particleVelocity[j]
					+ c1 * randDouble1 * (particleBestPosition[j] - particlePosition[j])
					+ c2 * randDouble2 * (gPosition[j] - particlePosition[j]);
		}

		s.getParticle(i).setCurrentVelocity(newVel);

	}

	public void updatePosition(Swarm s, int i) {

		double[] particlePosition = s.getParticle(i).getCurrentPosition();
		double[] particleVelocity = s.getParticle(i).getVelocity();
		double[] newPos = new double[s.getNumDims()];

		for (int j = 0; j < s.getNumDims(); j++) {
			newPos[j] = particlePosition[j] + particleVelocity[j];
			double pMax = fn.getMax();
			double pMin = fn.getMin();

			if (newPos[j] > pMax) {
				newPos[j] = pMax;
			} else if (newPos[j] < pMin) {
				newPos[j] = pMin;
			}
		}
		s.getParticle(i).setCurrentPosition(newPos);

	}

	private double summation(int n) {

		double counter = 0;

		for (int i = 1; i <= n; i++) {
			counter += 1.0 / i;
		}

		double result = fnEvals / counter;// + ((maxIterations%counter == 0)
											// ? 0 : 1);

		return result;

	}

	private int checkStaticRegroup(int count, int max) {

		if ((count >= max) && (swarms.length != 1)) {

			if (logging) {
				System.out.println("Splitting...");
			}

			int numNewSwarms = swarms.length / numMerge;
			if (numNewSwarms > dimensions) {
				numNewSwarms = dimensions;
			}

			// Used to determine new structure
			int k1 = dimensions % numNewSwarms;
			int s1 = (int) Math.ceil((double) dimensions / (double) numNewSwarms);
			int s2 = (int) Math.floor((double) dimensions / (double) numNewSwarms);

			int[] dimPerSwarm = new int[numNewSwarms];

			for (int s = 0; s < numNewSwarms; s++) {
				if (s < k1) {
					dimPerSwarm[s] = s1;
				} else {
					dimPerSwarm[s] = s2;
				}
			}

			swarms = swarmManager.changeK(swarms, dimensions, dimPerSwarm);

			count = 0;
			kParts = swarms.length; // kParts *= parts;

			// System.out.println("kParts: "+kParts);

		} // end splitting

		return count;

	}

	private double calcDistFromLast(double[] last, double[] cur) {

		double dist = 0;

		for (int d = 0; d < last.length; d++) {
			double val = last[d] - cur[d];
			dist += Math.pow(val, 2);
		}
		dist = Math.sqrt(dist);

		return dist;

	}

	private void checkStagnated() {

		int numStagnated = stagnatedSwarms.size();

		if (stagnatedSwarms.size() != 0) {

			int numMerges = numStagnated;

			if (numStagnated % 2 != 0) {
				numMerges += 1;

				Random rand = new Random();

				int randS = rand.nextInt(swarms.length);

				while (stagnatedSwarms.contains(randS)) {
					randS = rand.nextInt(swarms.length);
				}
				stagnatedSwarms.add(randS);
				swarms[randS].numStagnate = maxStagnate; // dis guy about to get
															// merged anyways so
															// numStagnate
															// doesn't matter

				// If this guy breaks the order, just sort to avoid issues with
				// swapping
				if (randS < stagnatedSwarms.get(stagnatedSwarms.size() - 2)) {
					Collections.sort(stagnatedSwarms);
				}
			}

			// Print worst
			if (logging) {
				for (int w = 0; w < stagnatedSwarms.size(); w++) {
					System.out.print(stagnatedSwarms.get(w) + ", ");
				}
				System.out.println();
			}

			int numNewSwarms = swarms.length - (numMerges / 2);

			if (logging) {
				System.out.println(numStagnated + " swarms stagnated... so we will have " + swarms.length + " - "
						+ (numMerges / 2) + " = " + numNewSwarms);
			}
			int[] dimPerSwarm = new int[numNewSwarms];
			int dpsIndex = 0;

			// SWAP to change order of swarms
			for (int s = 0; s < stagnatedSwarms.size(); s += 2) {

				int firstSwarm = stagnatedSwarms.get(s);
				int secondSwarm = stagnatedSwarms.get(s + 1);

				Swarm nextToFirst = swarms[firstSwarm + 1];
				swarms[firstSwarm + 1] = swarms[secondSwarm];
				swarms[secondSwarm] = nextToFirst;

				//System.out.println("Swapping swarms " + (firstSwarm + 1) + " and " + secondSwarm);
			}

			for (int s = 0; s < swarms.length; s++) {

				Swarm curSwarm = swarms[s];

				if (curSwarm.numStagnate == maxStagnate && curSwarm.getNumDims() != dimensions) {

					//System.out.println("Merging " + s + "(" + curSwarm.getDistFromLast() + ") and " + (s + 1));

					// The other stagnated swarm should be already swapped next
					// to the first one
					dimPerSwarm[dpsIndex] = curSwarm.getNumDims() + swarms[s + 1].getNumDims();

					dpsIndex += 1;
					s += 1;

				} else {
					// System.out.println("Just including swarm "+s);
					dimPerSwarm[dpsIndex] = curSwarm.getNumDims();

					dpsIndex += 1;
				}

				// System.out.println("Check: swarm "+s+" stangated for
				// "+curSwarm.numStagnate);

				// First, make sure the curSwarm hasn't already been accounted
				// for
				/*
				 * if (!taken.contains(s)) { if (curSwarm.numStagnate ==
				 * maxStagnate && curSwarm.getNumDims() != dimensions) {
				 * 
				 * if (stagnatedSwarms.get(worstIndex) == s){ worstIndex += 1; }
				 * System.out.println("Merging "+s+"("+curSwarm.getDistFromLast(
				 * )+") and "+stagnatedSwarms.get(worstIndex)); // Now that you
				 * know it stagnated, find lowest-improving // swarm (use
				 * worstSwarms) dimPerSwarm[dpsIndex] = curSwarm.getNumDims() +
				 * swarms[stagnatedSwarms.get(worstIndex)].getNumDims(); // Keep
				 * track of this relation for later first[numPair] = s;
				 * second[numPair] = stagnatedSwarms.get(worstIndex);
				 * 
				 * taken.add(s); taken.add(worstSwarms[worstIndex]);
				 * 
				 * numPair += 1; dpsIndex += 1; worstIndex += 1;
				 * 
				 * } else { System.out.println("Just including swarm "+s);
				 * dimPerSwarm[dpsIndex] = curSwarm.getNumDims();
				 * 
				 * dpsIndex += 1; } }
				 */
			}

			if (logging) {
				System.out.println("Swarms stagnated, new structure:");
				for (int i = 0; i < dimPerSwarm.length; i++) {
					System.out.print(dimPerSwarm[i] + " ");
				}
				System.out.println();
			}

			swarms = swarmManager.changeK(swarms, dimensions, dimPerSwarm);

			kParts = dimPerSwarm.length;

			// Stagnated swarms have now be split/merged, so reset counter
			stagnatedSwarms.clear();
			numStagnated = 0;
		}
	}

	// Why not just calc this as you update swarm best?

	/*
	 * private int[] getWorstSwarms() {
	 * 
	 * int numZeros = 0; int numWorst = numStagnated;
	 * 
	 * if (numWorst % 2 != 0){ numWorst += 1; }
	 * 
	 * int[] worstSwarm = new int[numWorst]; double[] worstSwarmDist = new
	 * double[numWorst];
	 * 
	 * for (int i = 0; i < worstSwarm.length; i++) { worstSwarm[i] = -1;
	 * worstSwarmDist[i] = Double.MAX_VALUE; }
	 * 
	 * for (int s = 0; s < swarms.length; s++) {
	 * 
	 * Swarm curSwarm = swarms[s]; double distTravelled =
	 * curSwarm.getDistFromLast();
	 * 
	 * double curBestDist = distTravelled; int curBestIndex = -1;
	 * 
	 * System.out.println("Swarm "+s+" progressed a dist of "+distTravelled);
	 * 
	 * // Set bar at "distTravelled" and then search EVERY worstSwarm to // see
	 * // if u can find a better one for (int w = 0; w < worstSwarm.length; w++)
	 * { if (curBestDist < worstSwarmDist[w]) { curBestDist = worstSwarmDist[w];
	 * curBestIndex = w; } } // This means we found the worst swarm that had
	 * greater dist than // curSwarm if (curBestIndex != -1) {
	 * worstSwarm[curBestIndex] = s; worstSwarmDist[curBestIndex] =
	 * distTravelled; if (distTravelled == 0) { numZeros += 1; } }
	 * 
	 * // If you've filled all spots with completely stangated swarms // dont
	 * bother to continue searching if (numZeros == numStagnated) { break; } }
	 * 
	 * //Print worst for (int w = 0 ; w < worstSwarm.length ; w++){
	 * System.out.print(worstSwarm[w]+" ("+worstSwarmDist[w]+"), "); }
	 * System.out.println();
	 * 
	 * return worstSwarm;
	 * 
	 * }
	 */

}
