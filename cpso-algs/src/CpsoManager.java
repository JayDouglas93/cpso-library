import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

//import org.apache.commons.math3.stat.inference.MannWhitneyUTest;

import algorithms.*;
import functions.*;

public class CpsoManager {

	private HashMap<String, String> params;
	private double[] fitPerEval;
	private double[][] fitPerRun;
	private double[] endFitness;
	private double[][] algEndFitness;

	private String[] algs;

	private int numDims;
	private int runs;
	private boolean rotated;
	private boolean singleRun;

	private int runID;

	public CpsoManager(String[] args) {

		String paramFile = "params";
		// String otherParamFile = "paramfiles/CPSOSK-F1";

		params = new HashMap<String, String>();
		try {
			readParams(paramFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Random ID for Clustered Environment ID
		/*
		 * Random rand = new Random(); runID = rand.nextInt(Integer.MAX_VALUE);
		 */

		runs = Integer.parseInt(params.get("runs"));
		int fnEvals = Integer.parseInt(params.get("functionEvals"));
		rotated = Boolean.parseBoolean(params.get("rotated"));
		singleRun = Boolean.parseBoolean(params.get("singleRun"));

		if (singleRun) {
			runs = 1;
		}

		Algorithm alg = null;

		boolean useOtherParamFile = Boolean.parseBoolean(params.get("useOtherParamFile"));
		/*
		 * if (useOtherParamFile){ try { readParams(otherParamFile); } catch
		 * (IOException e) { e.printStackTrace(); } }
		 */

		String[] dims = params.get("dimensions").split(",");
		String[] fns = params.get("functions").split(",");
		algs = params.get("algorithms").split(",");

		if (args.length != 0) {
			dims = new String[] { args[2] };
			algs = new String[] { args[0] };
			fns = new String[] { args[1] };
		}

		for (int d = 0; d < dims.length; d++) {

			numDims = Integer.parseInt(dims[d]);
			params.replace("dimensions", dims[d]);

			for (int f = 0; f < fns.length; f++) {

				String fn = fns[f].trim();
				params.replace("functions", fn);

				String outputName = fn + "-" + numDims;
				if (rotated) {
					outputName += "-rotated";
				}

				algEndFitness = new double[algs.length][runs];

				for (int i = 0; i < algs.length; i++) {
					System.out.println("???");
					fitPerRun = new double[runs][fnEvals];
					fitPerEval = new double[fnEvals];
					endFitness = new double[runs];

					String whichAlg = algs[i].trim();
					params.replace("algorithms", whichAlg);

					switch (whichAlg) {
					case "CPSOSK":
						for (int r = 0; r < runs; r++) {

							alg = new CPSOSK(params);
							double[] fit = alg.getFitPerEval();
							addFitPerEval(fit);
							addFitPerRun(r, fit);
							endFitness[r] = fit[fit.length - 1];

						}
						break;
					case "CPSOHK":
						for (int r = 0; r < runs; r++) {

							alg = new CPSOHK(params);
							double[] fit = alg.getFitPerEval();
							addFitPerEval(fit);
							addFitPerRun(r, fit);
							endFitness[r] = fit[fit.length - 1];
						}
						break;

					case "CCPSO":
						for (int r = 0; r < runs; r++) {

							alg = new CCPSO(params);
							double[] fit = alg.getFitPerEval();
							addFitPerEval(fit);
							addFitPerRun(r, fit);
							endFitness[r] = fit[fit.length - 1];
						}
						break;

					case "CCPSO2":
						for (int r = 0; r < runs; r++) {

							alg = new CCPSO2(params);
							double[] fit = alg.getFitPerEval();
							addFitPerEval(fit);
							addFitPerRun(r, fit);
							endFitness[r] = fit[fit.length - 1];
						}
						break;

					case "MCPSO":
						for (int r = 0; r < runs; r++) {

							alg = new MCPSO(params);
							double[] fit = alg.getFitPerEval();
							addFitPerEval(fit);
							addFitPerRun(r, fit);
							endFitness[r] = fit[fit.length - 1];
						}
						break;

					case "DCPSO":
						for (int r = 0; r < runs; r++) {

							alg = new DCPSO(params);
							double[] fit = alg.getFitPerEval();
							addFitPerEval(fit);
							addFitPerRun(r, fit);
							endFitness[r] = fit[fit.length - 1];
						}
						break;
						
					case "MCPSO2":
						for (int r = 0; r < runs; r++) {

							alg = new MCPSO2(params);
							double[] fit = alg.getFitPerEval();
							addFitPerEval(fit);
							addFitPerRun(r, fit);
							endFitness[r] = fit[fit.length - 1];
						}
						break;

					case "DCPSO2":
						for (int r = 0; r < runs; r++) {

							alg = new DCPSO2(params);
							double[] fit = alg.getFitPerEval();
							addFitPerEval(fit);
							addFitPerRun(r, fit);
							endFitness[r] = fit[fit.length - 1];
						}
						break;
					case "AMCCPSO":
						for (int r = 0; r < runs; r++) {

							alg = new AMCCPSO(params);
							double[] fit = alg.getFitPerEval();
							addFitPerEval(fit);
							addFitPerRun(r, fit);
							endFitness[r] = fit[fit.length - 1];
						}
						break;
					case "NPSOCC":
						for (int r = 0; r < runs; r++) {

							alg = new NPSOCC(params);
							double[] fit = alg.getFitPerEval();
							addFitPerEval(fit);
							addFitPerRun(r, fit);
							endFitness[r] = fit[fit.length - 1];
						}
						break;
					case "CBPSO":
						for (int r = 0; r < runs; r++) {
							
							alg = new CBPSO(params);
							double[] fit = alg.getFitPerEval();
							addFitPerEval(fit);
							addFitPerRun(r, fit);
							endFitness[r] = fit[fit.length - 1];
							
						}
						break;
					}

					for (int j = 0; j < fitPerEval.length; j++) {
						fitPerEval[j] /= runs;
					}
					writeFnEvals(whichAlg, outputName, fn);

					// writeStats(whichAlg, outputName);

					// algEndFitness[i] = endFitness;

				}

				/*
				 * if (!singleRun) { statisticalTest(outputName); }
				 */
			}
		}

	}

	private void readParams(String file) throws IOException {

		BufferedReader br = new BufferedReader(new FileReader(file));
		params = new HashMap<String, String>();
		String line = br.readLine();
		boolean isDone = false;

		while (line != null) {
			line = line.trim();
			if (!line.isEmpty()) {
				if (line.charAt(0) != '#') {
					String[] param = line.split("=");
					System.out.println(param[0] + "=" + param[1]);
					params.put(param[0].trim(), param[1].trim());
				}
			}

			line = br.readLine();
		}

		br.close();
	}

	private void writeFnEvals(String algStr, String file, String fn) {
		
		for (int r = 0; r < runs; r++) {
			
			Random rand = new Random(); 
			runID = rand.nextInt(Integer.MAX_VALUE);
			
			// if (singleRun) {
			String newFile = file + "-" + runID;
			// }

			PrintWriter pw = null;
			FileWriter fw = null;
			BufferedWriter bw = null;

			try {
				String dir = "results/" + numDims + "/" + fn + "/" + algStr + "/";
				new File(dir).mkdirs();
				fw = new FileWriter(dir + newFile + ".txt", true);
				bw = new BufferedWriter(fw);
				pw = new PrintWriter(bw);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			pw.write(algStr + "\n");

			// double[] fitPerIt = alg.getFitPerIt();

			for (int i = 0; i < fitPerEval.length; i++) {
				pw.write(fitPerRun[r][i] + "\n");
			}

			pw.write("\n");

			pw.close();

			/*
			 * if (!singleRun) { rowToColumn(file); }
			 */
		}

	}

	private void writeStats(String algStr, String file) {

		PrintWriter pw = null;
		FileWriter fw = null;
		BufferedWriter bw = null;

		try {
			String dir = "results/stats/" + numDims + "/";
			new File(dir).mkdirs();
			fw = new FileWriter(dir + file + "-stats.txt", true);
			bw = new BufferedWriter(fw);
			pw = new PrintWriter(bw);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		pw.write(algStr + " ");

		// double[] fitPerIt = alg.getFitPerIt();

		for (int i = 0; i < endFitness.length; i++) {
			pw.write(endFitness[i] + " ");
		}

		pw.write("\n");

		pw.close();

	}

	/*private void statisticalTest(String file) {

		PrintWriter pw = null;
		FileWriter fw = null;
		BufferedWriter bw = null;

		try {
			String dir = "results/stats/" + numDims + "/";
			new File(dir).mkdirs();
			fw = new FileWriter(dir + file + "-test.txt", true);
			bw = new BufferedWriter(fw);
			pw = new PrintWriter(bw);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (int a = 0; a < algEndFitness.length - 1; a++) {
			for (int b = a + 1; b < algEndFitness.length; b++) {
				// Verify
				MannWhitneyUTest m = new MannWhitneyUTest();
				double p = m.mannWhitneyUTest(algEndFitness[a], algEndFitness[b]);
				double meanA = mean(algEndFitness[a]);
				double meanB = mean(algEndFitness[b]);

				String symb;
				if (p <= 0.05) {
					if (meanA < meanB) {
						symb = "<";
					} else {
						symb = ">";
					}
				} else {
					symb = "=";
				}
				pw.println(algs[a] + "\t" + symb + "\t" + algs[b]);
				pw.println(meanA + "\t" + meanB);
				pw.println(p);

			}
		}

		pw.write("\n");

		pw.close();
	}*/

	private void rowToColumn(String file) {

		BufferedReader br = null;
		PrintWriter newPW = null;
		FileWriter fw = null;
		BufferedWriter bw = null;

		try {
			br = new BufferedReader(new FileReader("results/" + numDims + "/" + file + ".txt"));
			fw = new FileWriter("results/" + numDims + "/" + file + "-col.txt", false);
			bw = new BufferedWriter(fw);
			newPW = new PrintWriter(bw);

			String line;

			ArrayList<String[]> lines = new ArrayList<String[]>();
			while ((line = br.readLine()) != null) {
				String[] stringLine = line.split(" ");
				// System.out.println(stringLine.length);
				lines.add(stringLine);
			}

			for (int i = 0; i < lines.get(0).length; i++) {
				for (int l = 0; l < lines.size(); l++) {
					newPW.write(lines.get(l)[i] + " ");
				}
				newPW.write("\n");
			}

			newPW.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addFitPerEval(double[] fitness) {
		for (int i = 0; i < fitness.length; i++) {
			fitPerEval[i] += fitness[i];
		}
	}

	private void addFitPerRun(int r, double[] fitness) {
		for (int i = 0; i < fitness.length; i++) {
			fitPerRun[r][i] = fitness[i];
		}
	}

	public double mean(double[] arr) {
		double val = 0;
		for (int i = 0; i < arr.length; i++) {
			val += arr[i];
		}
		val /= arr.length;
		return val;
	}

	public static void main(String[] args) {
		CpsoManager cm = new CpsoManager(args);

	}

}
