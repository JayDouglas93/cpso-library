import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import algorithms.*;
import functions.*;

public class CompileRuns {

	private HashMap<String, String> params;
	private double[][] fitPerAlg;
	private double[] endFitness;
	private double[][] algEndFitness;

	private String[] algs;

	private int numDims;
	private boolean rotated;
	private boolean singleRun;

	private int runID;
	private int numCompile;
	private int runs;
	private int fnEvals;

	public CompileRuns() {

		String paramFile = "paramfiles/CPSOSK-F1";
		try {
			readParams(paramFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Random ID for Clustered Environment ID
		Random rand = new Random();
		runID = rand.nextInt(Integer.MAX_VALUE);

		runs = Integer.parseInt(params.get("runs"));
		fnEvals = Integer.parseInt(params.get("functionEvals"));
		rotated = Boolean.parseBoolean(params.get("rotated"));
		singleRun = Boolean.parseBoolean(params.get("singleRun"));
		numCompile = Integer.parseInt(params.get("compileRuns"));

		/*
		 * if (singleRun){ runs = 1; }
		 */

		Algorithm alg = null;

		String[] dims = params.get("dimensions").split(",");
		String[] fns = params.get("functions").split(",");
		algs = params.get("algorithms").split(",");

		for (int d = 0; d < dims.length; d++) {

			numDims = Integer.parseInt(dims[d]);
			params.replace("dimensions", dims[d]);

			for (int f = 0; f < fns.length; f++) {

				String fn = fns[f];
				params.replace("functions", fn);

				String path = "results/" + numDims + "/" + fn;
				if (rotated) {
					path += "-rotated";
				}
				
				String fileName = fn + "-" + numDims;
				if (rotated){
					fileName += "-rotated";
				}

				readFiles(fn, path, fileName);

				//statisticalTest(fileName);
			}
		}

	}

	private void readFiles(String fn, String path, String fileName) {

		fitPerAlg = new double[algs.length][fnEvals];
		algEndFitness = new double[algs.length][numCompile];
		
		File directory = new File(path);

		FileInputStream curFile;

		File[] fList = directory.listFiles();

		int runCount = 0;
		
		for (File file : fList) {

			if (file.isFile()) {

				
				try{
					curFile = new FileInputStream(path+"/"+file.getName());
					BufferedReader r = new BufferedReader(new InputStreamReader(curFile));
					
					for (int a = 0 ; a < algs.length ; a++){
						
						String[] line = r.readLine().split(" ");
						
						//for each fnEval, ignore first, its the alg name
						for (int i=0 ; i<fnEvals ; i++){
							fitPerAlg[a][i] += Double.parseDouble(line[i+1]);
						}
						algEndFitness[a][runCount] = Double.parseDouble(line[fnEvals-1]);
						
					}
					r.close();
					
				}
				catch(IOException e){
					e.printStackTrace();
				}
				
				runCount += 1;
			}
		}
		
		String newFile = "results/"+numDims+"/"+fileName+".txt";
		PrintWriter writer;
		try {
			writer = new PrintWriter(newFile, "UTF-8");
			for (int a=0 ; a < algs.length ; a++){
				
				writer.write(algs[a]+" ");
				
				for (int i=0 ; i<fnEvals ; i++){
					fitPerAlg[a][i] /= numCompile;
					
					if (fitPerAlg[a][i] == Double.POSITIVE_INFINITY){
						fitPerAlg[a][i] = Double.MAX_VALUE;
					}
					else if (fitPerAlg[a][i] == Double.NEGATIVE_INFINITY){
						fitPerAlg[a][i] = Double.MIN_VALUE;
					}
					writer.write(Double.toString(fitPerAlg[a][i])+" ");
				}
				writer.println();
			}
			
			writer.close();
			rowToColumn(fileName);
			
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void readParams(String file) throws IOException {

		params = new HashMap<String, String>();

		BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		while ((line = br.readLine()) != null) {
			String[] param = line.split("=");
			params.put(param[0], param[1]);
		}

		br.close();
	}

	private void rowToColumn(String file) {

		BufferedReader br = null;
		PrintWriter newPW = null;
		FileWriter fw = null;
		BufferedWriter bw = null;

		try {
			br = new BufferedReader(new FileReader("results/" + numDims + "/" + file + ".txt"));
			fw = new FileWriter("results/" + numDims + "/" + file + "-col.txt", false);
			bw = new BufferedWriter(fw);
			newPW = new PrintWriter(bw);

			String line;

			ArrayList<String[]> lines = new ArrayList<String[]>();
			while ((line = br.readLine()) != null) {
				String[] stringLine = line.split(" ");
				lines.add(stringLine);
			}

			for (int i = 0; i < lines.get(0).length; i++) {
				for (int l = 0; l < lines.size(); l++) {
					newPW.write(lines.get(l)[i] + " ");
				}
				newPW.write("\n");
			}

			newPW.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public double mean(double[] arr) {
		double val = 0;
		for (int i = 0; i < arr.length; i++) {
			val += arr[i];
		}
		val /= arr.length;
		return val;
	}

	public static void main(String[] args) {
		CompileRuns cr = new CompileRuns();

	}

}
