package functions;

public class Salomon extends Function {

	public Salomon(){
		pMin = -100;
		pMax = 100;
	}
	
	@Override
	public double eval(double[] vector) {

		double fitVal1 = 0;
		double fitVal2 = 0;

		for (int i = 0; i < vector.length; i++) {
			fitVal1 += Math.pow(vector[i], 2);
			fitVal2 += Math.pow(vector[i], 2);
		}

		double fitValTotal = (-1.0) * Math.cos(2 * Math.PI * fitVal1) + (0.1) * Math.sqrt(fitVal2) + 1;


		return fitValTotal;
	}

}
