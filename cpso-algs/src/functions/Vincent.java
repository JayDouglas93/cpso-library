package functions;

public class Vincent extends Function {

	public Vincent(){
		pMin = 0.25;
		pMax = 10;
	}
	
	@Override
	public double eval(double[] vector) {
		
		double fitVal = 0;
		double fitVal1 = 0;

		for (int i = 0; i < vector.length; i++) {
			fitVal1 += Math.sin(10.0 * Math.sqrt(vector[i]));
		}

		fitVal = -1.0 * (1.0 + fitVal1);

		return fitVal;
	}

}
