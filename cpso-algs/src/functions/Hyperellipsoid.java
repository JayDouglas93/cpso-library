package functions;

public class Hyperellipsoid extends Function {

	public Hyperellipsoid(){
		pMin = -5.12;
		pMax = 5.12;
	}
	
	@Override
	public double eval(double[] vector) {

		double fitValTotal = 0;

		for (int i = 0; i < vector.length; i++) {
			fitValTotal += (i + 1.0) * Math.pow(vector[i], 2);
		}

		return fitValTotal;
	}

}
