package functions;

public class Rosenbrock extends Function {

	public Rosenbrock(){
		pMin = -30;
		pMax = 30;
	}
	
	@Override
	public double eval(double[] vector) {
		
		if (isRotated){
			vector = multiplyVectorMatrix(vector, rotationMatrix);
		}
		
		double fitVal = 0;
		for (int i = 0; i < vector.length - 1; i++) {
			fitVal += (100.0 * Math.pow((vector[i + 1] - Math.pow(vector[i], 2.0)), 2.0)
					+ Math.pow((vector[i] - 1.0), 2.0));
		}
		
		return fitVal;
	}

}
