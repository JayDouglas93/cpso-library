package functions;

public class Griewank extends Function {

	public Griewank(){
		pMin = -600;
		pMax = 600;
	}
	
	@Override
	public double eval(double[] vector) {
		
		if (isRotated){
			vector = multiplyVectorMatrix(vector, rotationMatrix);
		}
		
		double fitValTotal = 0;

		// part 1
		double fitVal1 = 0;
		double fitVal2 = 1;
		for (int i = 0; i < vector.length; i++) {
			fitVal1 += Math.pow(vector[i], 2.0);
			fitVal2 *= Math.cos(vector[i] / Math.sqrt(i + 1.0));
			//// System.out.println(fitVal2+", "+fitVal1);
		}

		fitValTotal = (1.0 / 4000.0) * fitVal1 - fitVal2 + 1.0;
		
		return fitValTotal;
	}

}
