package functions;

public class Elliptic extends Function {

	public Elliptic(){
		pMin = -100;
		pMax = 100;
	}
	
	@Override
	public double eval(double[] vector) {
		
		if (isRotated){
			vector = multiplyVectorMatrix(vector, rotationMatrix);
		}
		
		double fitValTotal = 0;

		for (int i = 0; i < vector.length; i++) {
			fitValTotal += Math.pow(Math.pow(10.0, 6.0), ((i) / (vector.length-1))) * Math.pow(vector[i], 2); 
			// range: i=1...n_x, and mentions bottom is n_x - 1, so second last? System.out.println(fitValTotal);
		}
		
		return fitValTotal;
	}

}
