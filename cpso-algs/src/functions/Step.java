package functions;

public class Step extends Function{

	public Step(){
		pMin = -100;
		pMax = 100;
	}
	
	@Override
	public double eval(double[] vector) {
		
		double fitVal = 0;

		for (int i = 0; i < vector.length; i++) {
			fitVal += Math.pow(Math.floor(vector[i] + 0.5), 2);
		}

		return fitVal;
	}
	
}
