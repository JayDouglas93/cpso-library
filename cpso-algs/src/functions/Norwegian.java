package functions;

public class Norwegian extends Function {


	public Norwegian(){
		pMin = -1.1;
		pMax = 1.1;
	}
	
	@Override
	public double eval(double[] vector) {
		
		double fitValTotal = 1;

		for (int i = 0; i < vector.length; i++) {
			fitValTotal *= Math.cos(Math.PI * Math.pow(vector[i], 3)) * ((99 + vector[i]) / (100));
		}

		return fitValTotal;
	}

	
}
