package functions;

public class Weierstrass extends Function{

	public Weierstrass(){
		pMin = -0.5;
		pMax = 0.5;
	}
	
	@Override
	public double eval(double[] vector) {
		
		double fitVal = 0;
		double a = 0.5;
		double b = 3;

		for (int i = 0; i < vector.length; i++) {
			double fitVal1 = 0;
			for (int j = 1; j <= 20; j++) {
				fitVal1 += Math.pow(a, j) * Math.cos(2 * Math.PI * Math.pow(b, j) * (vector[i] + 0.5));
			}
			fitVal += fitVal1;
		}
		double fitVal2 = 0;
		for (int j = 1; j < 20; j++) {
			fitVal2 += Math.pow(a, j) * Math.cos(Math.PI * Math.pow(b, j));

		}

		double fitValTotal = fitVal - vector.length * fitVal2;

		return fitValTotal;
	}

}
