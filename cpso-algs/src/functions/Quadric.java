package functions;

public class Quadric extends Function {

	
	
	public Quadric(){
		pMin = -100;
		pMax = 100;
	}
	@Override
	public double eval(double[] vector) {
		
		double fitVal = 0;
		double fitVal2 = 0;
		for (int i = 0; i < vector.length; i++) {
			for (int j = 0; j < i; j++) {
				fitVal2 += vector[j];
			}
			fitVal += Math.pow(fitVal2, 2.0);
			fitVal2 = 0;
		}
		
		return fitVal;
	}

}
