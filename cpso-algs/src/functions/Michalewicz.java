package functions;

public class Michalewicz extends Function {

	public Michalewicz(){
		pMin = 0;
		pMax = Math.PI;
	}
	
	@Override
	public double eval(double[] vector) {

		double fitValTotal = 0;
		double m = 10;

		for (int i = 0; i < vector.length; i++) {
			fitValTotal += Math.sin(vector[i])
					* Math.pow(Math.sin((((i + 1) * Math.pow(vector[i], 2)) / (Math.PI))), 2.0 * m);
		}
		fitValTotal = fitValTotal * (-1.0);

		return fitValTotal;
	}

	
}
