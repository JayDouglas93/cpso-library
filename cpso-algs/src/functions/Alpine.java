package functions;

public class Alpine extends Function{

	public Alpine(){
		pMin = 0;
		pMax = 10;
	}
	
	@Override
	public double eval(double[] vector) {
		
		double fitValTotal = 1;

		for (int i = 0; i < vector.length; i++) {
			fitValTotal *= Math.sin(vector[i])*Math.sqrt(vector[i]);

		}

		return fitValTotal;
	}

}
