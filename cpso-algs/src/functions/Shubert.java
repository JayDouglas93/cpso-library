package functions;

public class Shubert extends Function {

	public Shubert(){
		pMin = -10;
		pMax = 10;
	}
	
	@Override
	public double eval(double[] vector) {
		
		double fitVal = 1;

		for (int i = 0; i < vector.length; i++) {
			double curVal = vector[i];
			double fitVal1 = 0;
			for (int j = 1; j <= 5; j++) {
				fitVal1 += j * Math.cos((j + 1) * curVal + j);
			}
			fitVal *= fitVal1;
		}

		return fitVal;
	}

	
	
}
