package functions;

public class Quartic extends Function{

	public Quartic(){
		pMin = -1.28;
		pMax = 1.28;
	}
	
	@Override
	public double eval(double[] vector) {

		double fitValTotal = 0;

		for (int i = 0; i < vector.length; i++) {
			fitValTotal += (i + 1) * Math.pow(vector[i], 4);
		}

		return fitValTotal;
	}

}
