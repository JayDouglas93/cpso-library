package functions;

public class Ackley extends Function {

	public Ackley(){
		pMin = -30;
		pMax = 30;
	}
	
	@Override
	public double eval(double[] vector) {
		
		if (isRotated){
			vector = multiplyVectorMatrix(vector, rotationMatrix);
		}
		
		double fitValTotal = 0;

		// part 1
		double fitVal1 = 0;
		double fitVal2 = 0;
		for (int i = 0; i < vector.length; i++) {
			fitVal1 += Math.pow(vector[i], 2);
			fitVal2 += Math.cos((double) 2.0 * Math.PI * vector[i]);
		}
		double oneOverDim = (1 / (double) vector.length);
		fitValTotal = -20.0 * Math.exp(-0.2 * Math.sqrt(oneOverDim * fitVal1)) - Math.exp(oneOverDim * fitVal2) + 20.0
				+ Math.E;
		
		return fitValTotal;
	}

}
