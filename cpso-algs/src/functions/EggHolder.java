package functions;

public class EggHolder extends Function {

	public EggHolder(){
		pMin = -512;
		pMax = 512;
	}
	
	@Override
	public double eval(double[] vector) {
		double fitValTotal = 0;

		for (int i = 0; i < vector.length - 1; i++) {
			double fitValTotal1 = -1.0 * (vector[i + 1] + 47.0)
					* Math.sin(Math.sqrt(Math.abs(vector[i + 1] + (vector[i] / 2.0) + 47.0)));
			double fitValTotal2 = Math.sin(Math.sqrt(Math.abs(vector[i] - (vector[i + 1] + 47.0))))
					* ((-1.0) * (vector[i]));
			fitValTotal += (fitValTotal1 + fitValTotal2);
		}

		return fitValTotal;
	}

}
