package functions;

public class AbsVal extends Function {

	public AbsVal(){
		pMin = -100;
		pMax = 100;
	}
	
	@Override
	public double eval(double[] vector) {
		
		double fitValTotal = 0;
		
		for (int i = 0; i < vector.length; i++) {
			fitValTotal += Math.abs(vector[i]);
		}
		
		return fitValTotal;
	}

}
