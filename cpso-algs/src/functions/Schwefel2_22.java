package functions;

public class Schwefel2_22 extends Function {

	public Schwefel2_22(){
		pMin = -10;
		pMax = 10;
	}
	
	@Override
	public double eval(double[] vector) {
		
		double fitVal = 0;
		double fitVal1 = 0;
		double fitVal2 = 1;

		for (int i = 0; i < vector.length; i++) {
			fitVal1 += Math.abs(vector[i]);
			fitVal2 *= Math.abs(vector[i]);
		}

		fitVal = fitVal1 + fitVal2;
		
		return fitVal;
	}

}
