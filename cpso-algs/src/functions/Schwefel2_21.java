package functions;

public class Schwefel2_21 extends Function {

	public Schwefel2_21(){
		pMin = -100;
		pMax = 100;
	}
	
	@Override
	public double eval(double[] vector) {
		
		double fitVal = Math.abs(vector[0]);
		
		for (int i = 1; i < vector.length; i++) {
			double nextVal = Math.abs(vector[i]);
			if (nextVal > fitVal) {
				fitVal = nextVal;
			}
		}
		
		return fitVal;
	}

}
