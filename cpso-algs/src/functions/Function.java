package functions;

import java.util.Random;

public abstract class Function {
	
	protected double pMin;
	protected double pMax;
	protected boolean isRotated;
	protected double[][] rotationMatrix;
	
	public abstract double eval(double[] vector);
	
	public double getPMin(){
		return pMin;
	}
	
	public double getPMax(){
		return pMax;
	}
	
	public void setRotated(boolean r){
		isRotated = r;
	}
	
	public double[] multiplyVectorMatrix(double[] v, double[][] m) {
		double[] newV = new double[v.length];
		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m[0].length; j++) {
				newV[i] += m[i][j] * v[j];
			}
		}

		return newV;
	}
	
	public double[][] getRandomRotation(int dim) {

		Random r = new Random();
		double[][] matrix = new double[dim][dim];

		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				matrix[i][j] = r.nextGaussian();
			}
			for (int j = 0; j < matrix[0].length; j++) {
				matrix[i][j] = matrix[i][j] / euclidean(matrix[i]);
			}
			double[] d = matrix[i];
			for (int j = 0; j <= i; j++) {
				double n = euclidean(d);
				double p = dotProduct(d, matrix[j]);
				for (int k = 0; k < matrix.length; k++) {
					d[k] = (d[k] - (p * matrix[j][k])) / Math.pow(n, 2);
				}
			}
			matrix[i] = divideVector(d, euclidean(d));
		}

		return matrix;
	}
	
	public void setRandomRotation(double[][] matrix){
		rotationMatrix = matrix;
	}
	
	private double euclidean(double[] vector) {
		double length = 0;
		for (int i = 0; i < vector.length; i++) {
			length += Math.pow(vector[i], 2);
		}

		return Math.sqrt(length);
	}

	private double dotProduct(double[] d1, double[] d2) {
		double val = 0;

		for (int i = 0; i < d1.length; i++) {
			val += d1[i] * d2[i];
		}

		return val;
	}
	
	private double[] divideVector(double[] v, double val) {
		double[] newV = new double[v.length];

		for (int i = 0; i < v.length; i++) {
			newV[i] = v[i] / val;
		}
		return newV;
	}
	
	
}
