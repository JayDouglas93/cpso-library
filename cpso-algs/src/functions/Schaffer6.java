package functions;

public class Schaffer6 extends Function{

	public Schaffer6(){
		pMin = -100;
		pMax = 100;
	}

	@Override
	public double eval(double[] vector) {

		double fitValTotal = 0;

		for (int i = 0; i < vector.length - 1; i++) {
			double fitValTop = Math.pow(Math.sin(Math.pow(vector[i], 2) + Math.pow(vector[i + 1], 2)), 2)
					- 0.5;
			double fitValBot = Math.pow(1 + 0.001 * (Math.pow(vector[i], 2) + Math.pow(vector[i + 1], 2)),
					2);
			fitValTotal += (0.5 + (fitValTop / fitValBot));
		}

		return fitValTotal;
	}
	
	
}
