package functions;

public class Spherical extends Function {

	public Spherical(){
		pMin = -5.12;
		pMax = 5.12;
	}
	
	@Override
	public double eval(double[] vector) {
		
		double fitVal = 0;

		for (int i = 0; i < vector.length; i++) {
			fitVal += Math.pow(vector[i], 2);
		}

		return fitVal;
	}

}
