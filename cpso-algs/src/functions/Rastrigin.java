package functions;

public class Rastrigin extends Function {

	private int A = 10;
	
	public Rastrigin(){
		pMin = -5.12;
		pMax = 5.12;
	}
	@Override
	public double eval(double[] vector) {
		
		if (isRotated){
			vector = multiplyVectorMatrix(vector, rotationMatrix);
		}
		
		double fitVal = A * vector.length;
		for (int i = 0; i < vector.length; i++) {
			fitVal += Math.pow(vector[i], 2.0) - A * Math.cos(2.0 * Math.PI * vector[i]);
		}
		
		return fitVal;
	}

}
