fns = "F2"
dims = "250"
set xlabel "Iterations"
set ylabel "Fitness"
FILES = system(dims."/ls -1 *.txt")
plot for [data in FILES] data u 1:2 w p pt 1 lt rgb 'black' notitle