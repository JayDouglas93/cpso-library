#!/bin/bash
algs=(CPSOSK CPSOHK CCPSO CBPSO CCPSO2 DCPSO MCPSO NPSOCC AMCCPSO DCPSO2 MCPSO2)
fns=(F1 F2 F3 F4 F5 F6 F7 F8 F9 F10 F11 F12 F13 F14 F15 F16 F17 F18 F19 F20)
dim="2000"
for fn in "${fns[@]}"; do
	for alg in "${algs[@]}"; do
		awk -f script.txt $dim/$fn/$alg/*.txt > $dim/$fn/$alg-$dim-$fn.txt
	done
	echo "ALGORS = 'CPSOSK CPSOHK CCPSO CCPSO2 DCPSO MCPSO NPSOCC AMCCPSO CBPSO DCPSO2 MCPSO2'; set key autotitle columnhead; set terminal png size 400,300; set output 'graphs/$dim/$fn-$dim.png'; plot for [algor in ALGORS] '$dim/$fn/'.algor.'-$dim-$fn.txt' with lines" | gnuplot
done